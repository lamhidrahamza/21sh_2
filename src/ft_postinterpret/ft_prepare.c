/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_prepare.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/22 19:49:01 by abiri             #+#    #+#             */
/*   Updated: 2019/07/25 23:39:48 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "arg_manipulation.h"
#include "ft_parse.h"
#include "ft_split_args.h"
#include "ft_prepare.h"

/*
**	this function adds a new command list to the commandline list
*/

t_interpret	*ft_add_commandline(t_cmdline *commands)
{
	t_interpret *inter;

	inter = ft_memalloc(sizeof(t_interpret));
	inter->next = NULL;
	if (commands->first == NULL)
	{
		commands->first = inter;
		commands->last = inter;
	}
	else if (commands->last)
	{
		commands->last->next = inter;
		commands->last = inter;
	}
	return (inter);
}

void	ft_add_command(t_interpret *cmdlist)
{
	t_command *cmd;

	cmd = ft_memalloc(sizeof(t_command));
	if (cmdlist->firstcmd == NULL)
	{
		cmdlist->firstcmd = cmd;
		cmdlist->lastcmd = cmd;
	}
	else if (cmdlist->lastcmd)
	{
		cmdlist->lastcmd->next = cmd;
		cmdlist->lastcmd = cmd;
	}
}

char	**ft_arg_to_tab(t_arg *args, size_t len)
{
	t_arg	*tofree;
	size_t	i;
	char	**finaltab;

	i = 0;
	if (len == 0)
		return (NULL);
	finaltab = ft_memalloc(sizeof(char *) * (len + 1));
	while (len--)
	{
		tofree = args;
		finaltab[i] = ft_strdup(args->content);
		args = args->next;
		free(tofree);
		i++;
	}
	return (finaltab);
}

/*
**	this function will gather the arguemnts of each command called
**		into the struct s_interpret, each command being a struct s_command
*/

t_interpret	*post_interpret_args(t_arg *args)
{
	t_cmdline	commands;
	t_arg		*arguments;
	t_interpret	*cmd;
	size_t		len;

	arguments = NULL;
	len = 0;
	ft_bzero(&commands, sizeof(t_cmdline));
	cmd = ft_add_commandline(&commands);
	ft_add_command(cmd);
	while (args && args->type)
	{
		if (args->type == TYPE_COMMAND)
		{
			ft_add_arg(&arguments, ft_new_arg(args->content, TYPE_COMMAND));
			len++;
		}
		else if (args->type == TYPE_PIPE || args->type == TYPE_SEMICOLON)
		{
			cmd->lastcmd->argv = ft_arg_to_tab(arguments, len);
			arguments = 0;
			len = 0;
			if (args->type == TYPE_SEMICOLON)
				cmd = ft_add_commandline(&commands);
			ft_add_command(cmd);
		}
		args = args->next;
	}
	cmd->lastcmd->argv = ft_arg_to_tab(arguments, len);
	return (commands.first);
}

/*
**	this function will parse the redirection and will return a t_redirect
**		struct with all the data about the redirection
*/

t_redirect	*ft_convert_redirection(t_arg *arg)
{
	t_redirect	*result;
	char		*redirection;

	result = ft_memalloc(sizeof(t_redirect));
	redirection = arg->content;
	if (ft_isdigit(*redirection))
	{
		result->fdfrom = ft_atoi(redirection);
		while (ft_isdigit(*redirection))
			redirection++;
	}
	else
		result->fdfrom = (*redirection == '>') ? 1 : 0;
	result->direction = (*redirection == '>') ? REDIR_OUT : REDIR_IN;
	while ((*redirection == '>' || *redirection == '<') && ++redirection)
		result->type++;
	if (*redirection == '&')
		result->fdto = (*(++redirection) == '-') ? NO_FD : ft_atoi(redirection);
	else
	{
		result->fdto = NO_FD;
		result->filename = ft_strdup(arg->next->content);
	}
	return (result);
}

/*
**	this function will add a redirection to the t_command struct
*/

void	ft_add_redirection(t_command *command, t_redirect *redir)
{
	if (!command->redirections)
	{
		command->redirections = redir;
		command->lastredirection = redir;
	}
	else
	{
		command->lastredirection->next = redir;
		command->lastredirection = redir;
	}
}

void	post_interpret_fd(t_interpret *cmd, t_arg *args)
{
	t_command *command;

	command = cmd->firstcmd;
	while (args)
	{
		if (args->type == TYPE_REDIRECT_IN || args->type == TYPE_REDIRECT_OUT)
			ft_add_redirection(command, ft_convert_redirection(args));
		else if (args->type == TYPE_PIPE)
			command = command->next;
		args = args->next;
	}
}

/*
int main(void)
{
	char	*line;
	t_arg	*args;
	t_interpret *command_list;
	t_redirect	*redir;

	get_next_line(0, &line);
	args = ft_splitargs(line);
	ft_parse_args(args);

	command_list = post_interpret_args(args);
	post_interpret_fd(command_list, args);

	t_command *cmd;

	cmd = command_list->firstcmd;
	while (cmd)
	{
		redir = cmd->redirections;
		while (redir)
		{
			ft_printf("redirect %d to (%d|%s)\n", redir->fdfrom, redir->fdto, redir->filename);
			redir = redir->next;
		}
		for (int i = 0; cmd->argv[i]; i++)
			ft_printf("{%s} ", cmd->argv[i]);
		ft_printf("\n");
		cmd = cmd->next;
	}
	return (0);
}*/
