/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_envbasic.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/20 18:13:50 by abiri             #+#    #+#             */
/*   Updated: 2019/03/25 21:29:19 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
**	this file contains basic functions to manipulate environment dictionnary
**	basic functions tested and proved working
*/

void		ft_dictfree(t_dict *dict)
{
	t_entry	*cursor;
	t_entry	*next;

	cursor = dict->first;
	while (cursor)
	{
		next = cursor->next;
		ft_envfree(cursor);
		cursor = next;
	}
	free(dict);
}

t_entry		*ft_newenv(const char *key, const char *value)
{
	t_entry *result;

	if (!(result = ft_memalloc(sizeof(t_entry))))
		return (NULL);
	result->key = ft_strdup(key);
	result->value = ft_strdup(value);
	result->next = NULL;
	return (result);
}

t_entry		*ft_addenv(t_entry *new, t_dict *head)
{
	if (head == NULL)
		return (NULL);
	if (head->last)
	{
		head->last->next = new;
		head->last = new;
	}
	else
	{
		head->first = new;
		head->last = new;
	}
	head->size++;
	return (new);
}

char		*ft_getenv(t_dict *env, char *key)
{
	t_entry *entry;

	entry = env->first;
	while (entry)
	{
		if (ft_strequ(entry->key, key))
			return (entry->value);
		entry = entry->next;
	}
	return (NULL);
}

void		ft_envfree(t_entry *entry)
{
	if (entry)
	{
		free(entry->key);
		free(entry->value);
	}
	free(entry);
}
