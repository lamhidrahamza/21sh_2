/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtin_cd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/21 18:31:05 by abiri             #+#    #+#             */
/*   Updated: 2019/03/26 11:57:25 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
**	ft_builtin_cd_oldpwd:
**	-	a function that gets the old pwd or printf an error and returns 0
*/

static	int	ft_builtin_cd_oldpwd(char **prev, char **newdir, t_dict *envlist)
{
	char *lastdir;

	if ((lastdir = ft_getenv(envlist, "OLDPWD")))
	{
		*prev = ft_strdup(lastdir);
		ft_putendl((*newdir = ft_strdup(lastdir)));
		return (1);
	}
	else
		ft_printf("cd: no path history\n");
	return (0);
}

/*
**	ft_cdfree:
**	-	a function that's only used to free some variables used for cd function
*/

static	int	ft_cdfree(char *a, char *b, char *c)
{
	free(a);
	free(b);
	free(c);
	return (1);
}

/*
**	ft_cdsetenv:
**	-	this function sets the environment variables PWD and OLDPWD
*/

static	int	ft_cdsetenv(char *current, t_dict *envlist)
{
	char *tofree;

	ft_setenv(envlist, "PWD", (tofree = getcwd(NULL, 0)));
	ft_setenv(envlist, "OLDPWD", current);
	free(tofree);
	return (0);
}

/*
**	ft_builtin_cd:
**	-	the main cd function that changes the current directory into the
**	argument given
**	-	the function supports normal directory change and - to return
*/

int			ft_builtin_cd(char **args, t_dict *envlist)
{
	char		*newdir;
	char		*current;
	char		*lastdir;

	if (!(lastdir = NULL) && ft_builtin_error(args, "cd", 0, 2))
		return (1);
	if ((current = getcwd(NULL, 0)) && !args[1]
			&& (newdir = ft_getenv(envlist, "HOME")))
		newdir = ft_strdup(newdir);
	else if (args[1] && ft_strequ(args[1], "-") &&
			!ft_builtin_cd_oldpwd(&lastdir, &newdir, envlist))
		return (ft_cdfree(current, NULL, lastdir));
	else if (args[1] && args[1][0] == '/')
		newdir = ft_strdup(args[1]);
	else if (args[1] && args[1][0] != '-')
		newdir = ft_strcjoin(current, '/', args[1]);
	if (chdir(newdir))
		ft_printf("cd: Invalid path\n");
	else
		ft_cdsetenv(current, envlist);
	return (ft_cdfree(current, newdir, lastdir));
}
