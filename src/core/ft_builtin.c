/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/19 13:19:48 by abiri             #+#    #+#             */
/*   Updated: 2019/07/16 12:12:43 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
**	ft_builtin_error:
**	-	this function is a standard function for all builtin functions
**	it displays errors when number of arguments is not ideal
*/

int	ft_builtin_error(char **args, char *progname, int min, size_t max)
{
	size_t len;

	if (!args || ((len = ft_tabsize(args)) < (size_t)min && min == 2))
	{
		ft_printf("%s: no arguments specified\n", progname);
		return (1);
	}
	else if (len < (size_t)min)
	{
		ft_printf("%s: not enough arguments\n", progname);
		return (1);
	}
	else if (len > (size_t)max)
	{
		ft_printf("%s: too many arguments\n", progname);
		return (1);
	}
	return (0);
}

/*
**	ft_builtin_setnev:
**	-	this function uses ft_envbasic.c functions to modify or add
**	an environment variable to the envlist
*/

int	ft_builtin_setenv(char **args, t_dict *envlist)
{
	char	**table;
	int		count;

	if (ft_builtin_error(args, "setenv", 2, 3))
		return (1);
	if ((count = ft_countchar(args[1], '=')) == 1 && !args[2])
	{
		table = ft_strsplit(args[1], '=');
		ft_setenv(envlist, table[0], table[1]);
		ft_tabfree(table);
	}
	else if (count == 0 && args[2])
		ft_setenv(envlist, args[1], args[2]);
	else
		ft_printf("setenv: Invalid syntax\n");
	return (1);
}

/*
**	ft_builtin_unsetenv:
**	-	this function deletes an existing environment variable
**	from the envlist
*/

int	ft_builtin_unsetenv(char **args, t_dict *envlist)
{
	if (ft_builtin_error(args, "unsetenv", 2, 2))
		return (1);
	if (!ft_strstr(args[1], "21sh"))
		ft_unsetenv(envlist, args[1]);
	else
		ft_printf("unsetenv: cannot delete shell variables\n");
	//ft_tabfree(args);
	return (1);
}

/*
**	ft_builtin:
**	-	this function checks if a given execname is a builtin function
**	then calls the correspoding function
*/

int		ft_echo(char **args)
{
	int i;

	i = 1;
	while(args[i])
	{
		ft_printf("%s", args[i]);
		if (args[i + 1])
			ft_putchar(' ');
		i++;
	}
	ft_putchar('\n');
	return (1);
}

int	ft_builtin(char **args, t_dict *envlist)
{
	if (args[0] == NULL)
	{
		ft_tabfree(args);
		return (1);
	}
	if (ft_strequ(args[0], "exit"))
		exit(0);
	if (ft_strequ(args[0], "cd"))
		return (ft_builtin_cd(args, envlist));
	if (ft_strequ(args[0], "echo"))
		return (ft_echo(args));
	if (ft_strequ(args[0], "env"))
		return (ft_env(envlist));
	else if (ft_strequ(args[0], "setenv"))
		return (ft_builtin_setenv(args, envlist));
	else if (ft_strequ(args[0], "unsetenv"))
		return (ft_builtin_unsetenv(args, envlist));
	else if (ft_strequ(args[0], "source"))
		return (ft_builtin_source(args, envlist));
	return (0);
}
