/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_interpret.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@pm.me>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/25 18:19:41 by abiri             #+#    #+#             */
/*   Updated: 2019/07/25 23:37:24 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "read_line.h"
#include "ft_interpret.h"
#include "ft_split_macros.h"

/*
**
**			JUST FOR DEBUG, REMOVE AFTER
**
**
*/

void	ft_debug(char *str)
{
	int fd = open("/dev/ttys000", O_WRONLY);
	ft_putstr_fd(str, fd);
	close(fd);
}

char	*ft_removeskip(char *line)
{
	char *result;

	result = ft_searchreplace(line, "\\;", ";");
	free(line);
	return (result);
}

/*
**	this function will copy the current file to the given pipe
*/

void	ft_copy_file_to_pipe(char *filename, int pipein[2])
{
	char	buff[100];
	int		ret;
	int		fd;

	if (!filename)
		return ;
	fd = open(filename, O_RDONLY);
	while ((ret = read(fd, buff, 100)))
		write(pipein[PIPE_W], buff, ret);
	close(fd);
}

/*
**	this function will copy the pipe content to the file
*/

void	ft_copy_pipe_to_file(char *filename, int pipein[2], char type)
{
	char	buff[100];
	int		temppipe[2];
	int		ret;
	int		fd;

	if (!filename)
		return ;
	fd = open(filename, (type == REDIR_DOUBLE) ? (O_CREAT |
				O_APPEND | O_WRONLY) : (O_CREAT | O_TRUNC | O_WRONLY),
			S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if (fd < 0)
		return ;
	pipe(temppipe);
	while ((ret = read(pipein[PIPE_R], buff, 100)))
	{
		write(temppipe[PIPE_W], buff, ret);
		write(fd, buff, ret);
	}
	close(pipein[PIPE_R]);
	pipein[PIPE_R] = temppipe[PIPE_R];
	pipein[PIPE_W] = temppipe[PIPE_W];
	close(temppipe[PIPE_W]);
	close(fd);
}

/*
**	this function link file descriptors to pipes and redirections
*/

void	ft_link_fd_in(t_redirect *redir, int pipein[2])
{
	int filefd;

	while (redir)
	{
		if (redir->direction != REDIR_IN)
		{
			redir = redir->next;
			continue ;
		}
		else if (redir->fdto == NO_FD && redir->filename)
			ft_copy_file_to_pipe(redir->filename, pipein);
		else if (redir->fdto != NO_FD)
			dup2(redir->fdto, redir->fdfrom);
		else if (redir->filename)
		{
			filefd = open(redir->filename, O_RDWR);
			dup2(filefd, redir->fdfrom);
		}
		else
			close(redir->fdfrom);
		redir = redir->next;
	}
}

/*
**	this function will link outgoing file descriptors
*/

void	ft_link_fd_out(t_redirect *redir, int pipeout[2])
{
	int filefd;

	if (pipeout[PIPE_W] != 1)
		close(pipeout[PIPE_W]);
	while (redir)
	{
		if (redir->direction != REDIR_OUT)
		{
			redir = redir->next;
			continue ;
		}
		if (redir->fdto == NO_FD && redir->filename)
			ft_copy_pipe_to_file(redir->filename, pipeout, redir->type);
		else if (redir->fdto != NO_FD)
			dup2(redir->fdto, redir->fdfrom);
		else if (redir->filename)
		{
			filefd = open(redir->filename, O_RDWR);
			dup2(filefd, redir->fdfrom);
		}
		else
			close(redir->fdfrom);
		redir = redir->next;
	}
}

/*
**	this function will tell if the first input is from standard input
*/

int		ft_is_stdin(t_command *cmd)
{
	t_redirect *redir;

	redir = cmd->redirections;
	while (redir)
	{
		if (redir->fdfrom == 0)
			return (0);
		redir = redir->next;
	}
	return (1);
}

/*
** this function will tell if the last element is connectes to the standard
** 	output
*/

int		ft_is_stdout(t_command *cmd)
{
	t_redirect *redir;

	redir = cmd->redirections;
	while (redir)
	{
		if (redir->fdfrom == 1)
			return (0);
		redir = redir->next;
	}
	return (1);
}

/*
**	this function will dup file descriptors and ppipes to the child process
*/

void	ft_link_fd(t_command *cmd, int pipefd[2][2])
{
	ft_link_fd_in(cmd->redirections, pipefd[PIPE_IN]);
	if (pipefd[PIPE_IN][PIPE_R] != 0)
		close(pipefd[PIPE_IN][PIPE_W]);
	dup2(pipefd[PIPE_IN][PIPE_R], 0);
	dup2(pipefd[PIPE_OUT][PIPE_W], 1);
}

/*
**	this function will execute commands one by one and will also
**		line pipes and redirections
*/

int	ft_execute(t_command *cmd, t_dict *envlist, t_interpret *cmdlist)
{
	int		id;
	char	*path;
	char	*envpath;
	char	**env;
	static	int	pipefd[2][2];

	env = NULL;
	envpath = ft_getenv(envlist, "PATH");
/*
	╔═╗┬─┐┌─┐┌─┐┌┬┐┬┌┐┌┌─┐  ┌─┐┬┌─┐┌─┐┌─┐
	║  ├┬┘├┤ ├─┤ │ │││││ ┬  ├─┘│├─┘├┤ └─┐
	╚═╝┴└─└─┘┴ ┴ ┴ ┴┘└┘└─┘  ┴  ┴┴  └─┘└─┘
*/
	if (cmd == cmdlist->firstcmd && ft_is_stdin(cmd))
		pipe(pipefd[PIPE_IN]);
	else if (cmd == cmdlist->firstcmd)
		pipefd[PIPE_IN][PIPE_R] = 0;
	if (cmd != cmdlist->lastcmd || !ft_is_stdout(cmd))
		pipe(pipefd[PIPE_OUT]);
	else
		pipefd[PIPE_OUT][PIPE_W] = 1;
//	ft_printf("%*@for command |%s|, stdin is %d\n", cGREEN, cmd->argv[0], pipefd[PIPE_IN][PIPE_R]);
//	ft_printf("for command |%s|, stdout is %d\n%@", cmd->argv[0], pipefd[PIPE_OUT][PIPE_W]);
/*
	╦┌┐┌  ╔═╗┬ ┬┬┬  ┌┬┐  ╔═╗┬─┐┌─┐┌─┐┌─┐┌─┐┌─┐
	║│││  ║  ├─┤││   ││  ╠═╝├┬┘│ ││  ├┤ └─┐└─┐
	╩┘└┘  ╚═╝┴ ┴┴┴─┘─┴┘  ╩  ┴└─└─┘└─┘└─┘└─┘└─┘
*/
	if ((path = ft_getpath(envpath, cmd->argv[0])))
	{
		g_sigintstat = 1;
		env = ft_getenvtab(envlist, path);
		id = fork();
		if (id == 0)
		{
			ft_link_fd(cmd, pipefd);
			execve(path, cmd->argv, env);
			exit(0);
		}
		else
		{
			if (pipefd[PIPE_IN][PIPE_R] != 0)
				close(pipefd[PIPE_IN][PIPE_W]);
			wait(NULL);
		}
	}
/*
	╔═╗┬ ┬┌─┐┌─┐┌─┐┬┌┐┌┌─┐  ╔═╗┬┌─┐┌─┐┌─┐
	╚═╗│││├─┤├─┘├─┘│││││ ┬  ╠═╝│├─┘├┤ └─┐
	╚═╝└┴┘┴ ┴┴  ┴  ┴┘└┘└─┘  ╩  ┴┴  └─┘└─┘
*/
	if (pipefd[PIPE_IN][PIPE_R] != 0)
		close(pipefd[PIPE_IN][PIPE_R]);
	ft_link_fd_out(cmd->redirections, pipefd[PIPE_OUT]);
	pipefd[PIPE_IN][PIPE_R] = pipefd[PIPE_OUT][PIPE_R];
	pipefd[PIPE_IN][PIPE_W] = pipefd[PIPE_OUT][PIPE_W];
	g_sigintstat = 0;
	free(path);
	ft_tabfree(env);
	return (0);
}

/*
**	this function frees the command redirections
*/

void	ft_free_redirections(t_redirect *redir)
{
	t_redirect *temp;

	while (redir)
	{
		temp = redir->next;
		free(redir->filename);
		free(redir);
		redir = temp;
	}
}

/*
**	this function frees the command line information
*/

void	ft_free_commandline(t_interpret *cmdlist)
{
	t_command *cmd;
	t_command *temp;

	cmd = cmdlist->firstcmd;
	while (cmd)
	{
		ft_tabfree(cmd->argv);
		temp = cmd->next;
		ft_free_redirections(cmd->redirections);
		free(cmd);
		cmd = temp;
	}
	free(cmdlist);
}

/*
**	this function gets a pipe entry or exit everytime it is called
**		depending on the state this it is called
*/

int	ft_get_pipe_end(int *toclose)
{
	static int	state = PIPE_IN;
	static int	pipe_var[2] = {-1, -1};
	int			temp;

	if (pipe_var[0] == -1 || pipe_var[1] == -1)
		pipe(pipe_var);
	if (state == PIPE_IN)
	{
		state = PIPE_OUT;
		*toclose = pipe_var[PIPE_OUT];
		return (pipe_var[PIPE_IN]);
	}
	else
	{
		state = PIPE_IN;
		temp = pipe_var[PIPE_OUT];
		*toclose = pipe_var[PIPE_IN];
		pipe_var[PIPE_IN] = -1;
		pipe_var[PIPE_OUT] = -1;
		return (temp);
	}
}

int	ft_isbuiltin(char *program)
{
	if (ft_strequ(program, "exit"))
		exit(0);
	if (ft_strequ(program, "cd") || ft_strequ(program, "echo") || ft_strequ(program, "setenv") || ft_strequ(program, "unsetenv") || ft_strequ(program, "source") || ft_strequ(program, "env"))
		return (1);
	return (0);
}

/*
**
**
**	FILE DESCRIPTOR AGREGATION AND REDIRECTIONS
**
**/

typedef struct	s_pipeline
{
	int			lastpid;
	int			lastpipe[2];
}				t_pipeline;

int	ft_fd_agregation(t_redirect *redir)
{
	if (redir->fdto != -1)
		dup2(redir->fdto, redir->fdfrom);
	else
		close(redir->fdfrom);
	return (0);
}

/*
int	ft_redirect_filein(t_redirect *redir, int fd_in, t_pipeline *pipes, int ispipe)
{
	int id;
	int ret;
	char buffer[201];
	int fd;
	static	int lastpid = -1;

	if (lastpid == -1)
		lastpid = pipes->lastpid;
	if ((fd = open(redir->filename, O_RDONLY)) < 0)
		return (0);
	if (!ispipe)
	{
		ft_printf("just duplicating\n");
		dup2(fd, redir->fdfrom);
		return (0);
	}
	ft_printf("creating sub process for redirection\n");
	id = fork();
	if (id == 0)
	{
		ft_printf("%*@waiting for pid : %d\n%@", cYELLOW, lastpid);
		waitpid(lastpid, NULL, 0);
		ft_printf("the wait ended\n");
		while ((ret = read(fd, buffer, 200)) > 0)
		{
			buffer[ret] = '\0';
			if (write(fd_in, buffer, ret) <= 0)
				exit(0);
			ft_printf("still reading\n");
		}
		close(fd);
		close(fd_in);
		exit(0);
	}
	lastpid = id;
	return (0);
}

int	ft_redirect_fileout(t_redirect *redir, int fd_out, t_pipeline *pipes, int ispipe)
{
	int id;
	int ret;
	char buffer[201];
	int fd;
	static	int lastpid = -1;

	if (lastpid == -1)
		lastpid = pipes->lastpid;
	if ((fd = open(redir->filename, O_WRONLY | O_CREAT | O_TRUNC)) < 0)
	{
		ft_printf("cannot open the damn file\n");
		return (0);
	}
	if (!ispipe)
	{
		ft_printf("just duplicating\n");
		dup2(fd, redir->fdfrom);
		return (0);
	}
	ft_printf("creating sub process for redirection\n");
	waitpid(lastpid, NULL, 0);
	id = fork();
	if (id == 0)
	{
		ft_printf("%*@waiting for pid : %d\n%@", cYELLOW, lastpid);
		ft_printf("reading from file descriptor : %d\n", fd_out);
		while ((ret = read(fd_out, buffer, 200)) > 0)
		{
			buffer[ret] = '\0';
			if (write(fd, buffer, ret) <= 0)
				exit(0);
		}
		close(fd);
		close(fd_out);
		exit(0);
	}
	lastpid = id;
	return (0);
}
*/

int	ft_heredoc(t_redirect *redir)
{
	t_history	hist;
	t_select	select;
	char		*buffer;
	char		*heredoc;
	int			pipe_fd[2];

	select.start = -1;
	select.end = -1;
	select.save = NULL;
	hist.history = ft_alloc_tab();
	hist.his_count = 0;
	buffer = NULL;
	heredoc = ft_strdup("");
	while (!buffer || ft_strcmp(buffer, redir->filename))
	{
		ft_putstr("heredoc> ");
		buffer = ft_read_line(&hist, &select, 9);
		heredoc = ft_strfreecjoin(heredoc, '\n', buffer);
	}
	heredoc = ft_strfreejoin(heredoc, ft_strdup("\n"));
	pipe(pipe_fd);
	write(pipe_fd[PIPE_IN], heredoc, ft_strlen(heredoc));
	close(pipe_fd[PIPE_IN]);
	dup2(pipe_fd[PIPE_OUT], 0);
	free(heredoc);
	return (1);
}

int	ft_redirect_filein(t_redirect *redir)
{
	int		fd;

	fd = -1;
	if (redir->type == REDIR_SIMPLE)
		fd = open(redir->filename, O_RDONLY);
	else
		return (ft_heredoc(redir));
	if (fd < 0)
		return (0);
	dup2(fd, redir->fdfrom);
	return (1);
}

int	ft_redirect_fileout(t_redirect *redir)
{
	int fd;

	if (redir->type == REDIR_SIMPLE)
		fd = open(redir->filename, O_WRONLY | O_CREAT | O_TRUNC, 0644);
	else
		fd = open(redir->filename, O_WRONLY | O_CREAT | O_APPEND, 0644);
	if (fd < 0)
		return (0);
	dup2(fd, redir->fdfrom);
	return (1);
}

int	ft_connect_redirections(t_redirect *redir)
{
	while (redir)
	{
//		ft_printf("redirection : from : %d, to : %d, direction %d, type %d, filename %s\n", redir->fdfrom, redir->fdto, (int)redir->direction, (int)redir->type, redir->filename);
		if (!redir->filename)
			ft_fd_agregation(redir);
		else if (redir->filename && redir->direction == REDIR_IN)
			ft_redirect_filein(redir);
		else if (redir->filename && redir->direction == REDIR_OUT)
			ft_redirect_fileout(redir);
		redir = redir->next;
	}
	return (0);
}

/*
**
**
**	PIPELINE SYSTEM
**
*/

int	ft_execute_command(t_command *cmd, int position, char *envpath, t_dict *envlist)
{
	int id;
	int fd_in = 0;
	int fd_out = 1;
	int	close_in;
	int	close_out;
	char *path;
	char **env;

	if (!cmd->argv)
		return (0);
	if (ft_isbuiltin(cmd->argv[0]))
		return (ft_builtin(cmd->argv, envlist));
	env = ft_getenvtab(envlist, envpath);
	path = ft_getpath(envpath, cmd->argv[0]);
	close_in = -1;
	close_out = -1;
	if (!(position & CMD_FIRST))
		fd_in = ft_get_pipe_end(&close_in);
	if (!(position & CMD_LAST))
		fd_out = ft_get_pipe_end(&close_out);
	if (!path)
	{
		ft_printpathstatus(0, cmd->argv[0]);
		return (0);
	}
	id = fork();
	if (id == 0)
	{
		dup2(fd_in, 0);
		dup2(fd_out, 1);
		ft_connect_redirections(cmd->redirections);
		close(close_in);
		close(close_out);
		if (path)
			execve(path, cmd->argv, env);
		exit(0);
	}
	else
	{
		if (fd_in != 0)
			close(fd_in);
		if (fd_out != 1)
			close(fd_out);
		if (position & CMD_LAST || ((cmd->redirections && cmd->redirections->fdfrom == 1  && cmd->redirections->fdto != 1)))
		{
			g_sigintstat = 1;
			waitpid(id, NULL, 0);
			g_sigintstat = 0;
			kill(KILL_CHILDREN, SIGQUIT);
		}
		ft_tabfree(env);
	}
	return (0);
}

/*
**	this function loops thru all the commands that need to be executed
**		and then gets them all linked with pipes and executed
*/

int	ft_shell_execute(t_interpret *cmdlist, t_dict *envlist)
{
	t_command	*cmd;
	char		*envpath;
	int position;

	cmd = cmdlist->firstcmd;
	envpath = ft_getenv(envlist, "PATH");
	while (cmd && cmd->argv && cmd->argv[0])
	{
		position = 0;
		if (cmd == cmdlist->firstcmd)
			position |= CMD_FIRST;
		if (cmd == cmdlist->lastcmd)
			position |= CMD_LAST;
		ft_execute_command(cmd, position, envpath, envlist);
		
		cmd = cmd->next;
	}
	ft_free_commandline(cmdlist);
	return (0);
}

/*
**	this function is responsible for executing the commands one by one
**		it gets the t_interpret struct containing information
**		about every command including the argv, and redirections
*/

/*
int	ft_shell_execute(t_interpret *cmdlist, t_dict *envlist)
{
	t_command *cmd;

	cmd = cmdlist->firstcmd;
	while (cmd)
	{
		if (!ft_builtin(cmd->argv, envlist))
			ft_execute(cmd, envlist, cmdlist);
		cmd = cmd->next;	
	}
	ft_free_commandline(cmdlist);
	return (0);
}
*/

/*
**	This function will free the list of args after they are converted
**		to tokens
*/

void	ft_free_args(t_arg *args)
{
	t_arg *temp;

	while (args)
	{
		temp = args->next;
		free(args->content);
		free(args);
		args = args->next;
	}
}

/*
**	This function returns the variable name given the string and the
**		index of the $ sign
*/

char	*ft_get_varname(char *line, size_t index)
{
	size_t start;
	size_t len;

	start = ++index;
	len = 0;
	while (line[index])
	{
		if (!ft_isalnum(line[index]) && line[index] != '_')
			break;
		len++;
		index++;
	}
	return (ft_strsub(line, start - 1, len + 1));
}

/*
**	This function calculates the space needed for an escaped variable
**		to be allocated
*/

size_t	ft_varsize(char *var, int quote)
{
	size_t index;
	size_t size;

	index = 0;
	size = 0;
	while (var[index])
	{
		if (!quote && ISSEP(var, index))
			size++;
		else if (!quote && ISESC(var, index))
			size++;
		size++;
		index++;			
	}
	return (size);
}

/*
**	This function replaces the given variable with the given value
*/

char	*ft_replace_var(char *line, size_t index, t_dict *envlist, int quote)
{
	char	*temp;
	char	*result;
	char	*varname;
	size_t	i;
	size_t	j;

	i = 0;
	j = 0;
	result = NULL;
	varname = ft_get_varname(line, index);
	temp = ft_getenv(envlist, varname + 1);
	if (temp)
	{
		result = ft_memalloc(ft_varsize(temp, quote) + 1);
		while (temp[i])
		{
			if (!quote && (ISSEP(temp, i) || ISESC(temp, i)))
			{
				result[j++] = '\\';
				result[j++] = temp[i];
			}
			else
				result[j++] = temp[i];
			i++;
		}
		temp = ft_strreplace(line, varname, result, index);
	}
	else
		temp = ft_strreplace(line, varname, "", index);
	free(varname);
	free(result);
	free(line);
	return (temp);
}

/*
**	This function replaces the tilda HOME variable
*/

char	*ft_replace_tilda(char *line, t_dict *envlist, int index)
{
	char	*value;

	value = ft_getenv(envlist, "HOME");
	if (index != 0 && !ISSEP(line, index - 1) && !ISQUOT(line, index - 1))
		return (line);
	if (value)
		line = ft_strreplace(line, "~", value, index);
	return (line);
}

/*
**	This function replaces variables in the current command line
*/

char	*ft_varreplace(char *line, t_dict *envlist)
{
	size_t	index;
	char	quote;

	index = 0;
	while (line[index])
	{
		if (ISQUOT(line, index))
		{
			if (!INQUOTE && (SETINQUOTE))
				SETQUOTE(line[index]);
			else if (INQUOTE && (UNSETINQUOTE))
				SETQUOTE(0);
		}
		else if (line[index] == '~' && (!INQUOTE || LASTQUOTE == '"'))
			line = ft_replace_tilda(line, envlist, index);
		else if (line[index] == '$' && ft_fromcharset(line, index, "$"))
		{
			if (!INQUOTE || LASTQUOTE == '"')
				line = ft_replace_var(line, index, envlist, INQUOTE);
		}
		index++;
	}
	return (line);
}


/*
**	This function unescapes args before they are given to post processing
*/

void	ft_unescape_args(t_arg *args)
{
	char *result;
	while (args)
	{
		result = ft_strunescape(args->content);
		free(args->content);
		args->content = result;
		args = args->next;
	}
}

/*
**	This function will act like a bus function that will link
**		and permit data transfer from one step to another
**		and will also be the main function that will call
**		all other functions related to interpreting
*/

int	ft_shellmain(char **line, t_dict *envlist)
{
	t_arg		*args;
	t_interpret	*cmdlist;
	char		*temp;
	int			status;

	temp = *line;
	*line = ft_varreplace(*line, envlist);
	args = ft_splitargs(*line, &status);
	if (!args)
		return (PARSE_OK);
	if (status)
	{
		ft_free_args(args);
		if (status == '\'')
			return (SPLIT_MULTILINE);
		else if (status == '"')
			return (SPLIT_MULTILINE_D);
	}
	status = ft_parse_args(args);
	if (status != 1)
	{
		ft_free_args(args);
		return (status);
	}
	ft_unescape_args(args);
	
	cmdlist = post_interpret_args(args);
	post_interpret_fd(cmdlist, args);
	ft_free_args(args);
	
	while (cmdlist)
	{
		ft_shell_execute(cmdlist, envlist);
		cmdlist = cmdlist->next;
	}
	return (PARSE_OK);
}

void	ft_interpret(char *line, t_dict *envlist)
{
	char	**commands;
	char	**table;
	char	**args;
	int		index;

	ft_putendl("lalalalal");
	index = -1;
	if (!line)
		return ;
	commands = ft_strsplitescape(line, ';');
	while (commands[++index])
	{
		ft_putendl(commands[index]);
		commands[index] = ft_removeskip(commands[index]);
		args = ft_getargs(commands[index]);
		if (!(args = ft_getalias(args)))
			continue ;
		table = ft_shellvars(args, envlist);
		if (!ft_builtin(table, envlist))
			ft_exec(table, envlist);
	}
	ft_tabfree(commands);
}
