/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cmdline.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/17 20:04:33 by abiri             #+#    #+#             */
/*   Updated: 2019/07/16 12:32:24 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
**	this file contains functions to split arguments and replace variables
**	functions that are part of the preprocessing of the command
*/


/*
**	this function must be changed, it splits args into an array
*/

char		**ft_getargs(char *cmd)
{
	char	**result;
	int		i;
	int		inquote;
	char	lastquote;

	i = -1;
	inquote = 0;
	lastquote = 0;
	cmd = ft_strdup(cmd);
	while (cmd[++i])
	{
		if (!inquote && ft_ischarin(WHITESPACES, cmd[i]) && (cmd[i] = -1))
			continue ;
		if (!inquote && (cmd[i] == 39 || cmd[i] == '"'))
			lastquote = cmd[i];
		else if (inquote && cmd[i] == lastquote)
			lastquote = 0;
		else
			continue ;
		cmd[i] = -2;
		inquote = (inquote) ? 0 : 1;
	}
	result = ft_strsplit(ft_removechars(&cmd, -2), -1);
	free(cmd);
	return (result);
}

/*
**	ft_replacevars:
**	-	this function replaces environment variables int the user input
*/

char		*ft_replacevars(char *str, t_dict *envlist)
{
	char *parsed;
	char *final;
	char *old;
	char *rep;

	final = ft_strdup(str);
	while ((parsed = ft_getnextvar(final)))
	{
		old = final;
		if ((rep = ft_getenv(envlist, parsed + 1)))
			final = ft_searchreplace(final, parsed, rep);
		else
			final = ft_searchreplace(final, parsed, "");
		free(parsed);
		free(old);
	}
	old = final;
	final = ft_searchreplace(final, "\\$", "$");
	free(old);
	return (final);
}

/*
**	ft_shellvars:
**	-	this function replaces the ~ variable and calls ft_replacevars
*/

char		**ft_shellvars(char **args, t_dict *envlist)
{
	int		i;
	char	*old;
	char	*new;

	i = -1;
	while (args[++i])
	{
		if (ft_ischarin(args[i], '~') && (new = ft_getenv(envlist, "HOME")))
		{
			old = args[i];
			args[i] = ft_searchreplace(args[i], "~", new);
			free(old);
		}
		old = args[i];
		args[i] = ft_replacevars(old, envlist);
		free(old);
	}
	return (args);
}

/*
**	ft_verifpath:
**	-	verifies the path and exec rights then gives available paths
**	to be tested
*/

int			ft_verifpath(char *execname, char **possible,
		char ***available, char *envpath)
{
	int status;

	status = 0;
	if ((execname[0] == '.' || execname[0] == '/') &&
			(status = ft_execright(execname)) == 1)
	{
		*possible = ft_strdup(execname);
		return (1);
	}
	if (status == -1)
	{
		ft_printf("21sh: permission denied\n");
		return (-1);
	}
	if (!envpath)
		return (-1);
	*available = ft_strsplit(envpath, ':');
	return (0);
}

/*
**	ft_getpath:
**	-	looks for the execname in the PATH variable directories
**	then returns the path of the executable or NULL if not found
*/

char		*ft_getpath(char *envpath, char *execname)
{
	char	**available;
	char	*possible;
	int		i;
	int		status;

	i = -1;
	if ((status = ft_verifpath(execname, &possible, &available, envpath)) == 1)
		return (possible);
	else if (status == -1)
		return (NULL);
	while (available[++i])
	{
		possible = ft_strcjoin(available[i], '/', execname);
		if ((status = ft_execright(possible)) == 1)
		{
			ft_tabfree(available);
			return (possible);
		}
		else if (status == -1)
			break ;
		free(possible);
	}
	ft_tabfree(available);
	return (NULL);
}
