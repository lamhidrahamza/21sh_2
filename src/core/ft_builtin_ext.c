/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtin_ext.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/21 00:42:51 by abiri             #+#    #+#             */
/*   Updated: 2019/02/15 16:40:23 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
**	ft_builtin_source:
**	-	a builtin function that sources the file given as a config file
*/

int		ft_builtin_source(char **args, t_dict *envlist)
{
	char *prompt;

	if (ft_builtin_error(args, "source", 2, 2))
		return (1);
	if (args[1] && access(args[1], X_OK))
		ft_source(envlist, args[1]);
	else
		ft_printf("source: file does not exist or is inaccessible\n");
	if ((prompt = ft_getenv(envlist, "MINISHELL_PROMPT")))
		g_prompt = prompt;
	return (1);
}
