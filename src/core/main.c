/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/17 20:55:00 by abiri             #+#    #+#             */
/*   Updated: 2019/07/18 12:49:37 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "ft_parse.h"
#include "read_line.h"

/*
**	ft_exec:
**	-	executes a command given the envlist and the args
*/

void	ft_exec(char **args, t_dict *envlist)
{
	int		id;
	char	*path;
	char	*envpath;
	char	**env;

	env = NULL;
	envpath = ft_getenv(envlist, "PATH");
	if ((path = ft_getpath(envpath, args[0])))
	{
		g_sigintstat = 1;
		env = ft_getenvtab(envlist, path);
		id = fork();
		if (id == 0)
		{
			execve(path, args, env);
			exit(0);
		}
		else
			wait(NULL);
	}
	g_sigintstat = 0;
	free(path);
	ft_tabfree(env);
	ft_tabfree(args);
	return ;
}

/*
**	ft_source:
**	-	opens the file given as a parameter
**	-	reads every command and interprets it
*/

void	ft_source(t_dict *envlist, char *otherpath)
{
	char	*line;
	char	*path;
	int		fd;

	if (!otherpath)
		path = ft_strcjoin(ft_getenv(envlist, "HOME"), '/', ".21shrc");
	else
		path = ft_strdup(otherpath);
	fd = open(path, O_RDONLY);
	while (get_next_line(fd, &line) > 0)
	{
		ft_shellmain(&line, envlist);
		free(line);
	}
	free(path);
}

/*
**	ft_shellloop:
**	-	interprets the command specified in the config file
**	-	prints the prompt
**	-	gets the input
**	-	interprets it with ft_interpret
*/

void	ft_shelloop(t_dict *envlist, char *prompt_cmd)
{
	t_history	his;
	t_select	select;
	char	*line;
//	char	*oldline;
	int		parsestatus;

	select.start = -1;
	select.end = -1;
	select.save = NULL;
	his.history = ft_alloc_tab();
	his.his_count = 0;
	his.path = ft_getenv(envlist, "PATH");
	parsestatus = PARSE_OK;
	while (1)
	{
		prompt_cmd = NULL;
		ft_startc(clYELLOW);
		//ft_interpret(prompt_cmd, envlist);
		ft_endc();
		ft_printf("%*@%s%@ ", clCYAN, g_prompt);
		line = ft_read_line(&his, &select, 3);
		if (ft_strcmp(line, ""))
		{
			ft_stock_history(his.history, line, his.his_count);
			if (his.his_count < MAX_HISTORY)
				his.his_count++;
			parsestatus = ft_shellmain(&line, envlist);
			while (parsestatus > 1 && parsestatus < 5)
			{
				ft_quotes(&line, &select, parsestatus);
				parsestatus = ft_shellmain(&line, envlist);
			}

		}
/*		if (line)
		{
			if (parsestatus == PARSE_INCOMPLETE)
				line = ft_strfreejoin(oldline, line);
			if (parsestatus == SPLIT_MULTILINE)
				line = ft_strfreecjoin(oldline, '\n', line);
	   		parsestatus = ft_shellmain(&line, envlist);
		}
		if (parsestatus != PARSE_INCOMPLETE && parsestatus != SPLIT_MULTILINE)
		{
			g_prompt = "$>";
			free(line);
		}
		else
		{
			g_prompt = "..";
			oldline = line;
		}*/
	}
}

/*
**	ft_loadenv:
**	-	loads environ into a linked list
**	containing pairs of key - value
*/

t_dict	*ft_loadenv(char **env)
{
	int		i;
	char	**table;
	t_dict	*envvar;

	i = 0;
	envvar = malloc(sizeof(t_dict));
	envvar->size = 0;
	envvar->first = NULL;
	envvar->last = NULL;
	while (env[i])
	{
		table = ft_strsplit(env[i], '=');
		ft_addenv(ft_newenv(table[0], table[1]), envvar);
		ft_tabfree(table);
		i++;
	}
	return (envvar);
}

/*
**	main function:
**	-	loads environment variables into envvar
**	-	sources the config file at ~/.21shrc
**	-	sources file if given as argument
**	-	shows usage when necessary
**	-	call tht shell loop function
*/

int		main(int argc, char **argv, char **env)
{
	t_dict	*envvar;

	ft_setsignals();
	envvar = ft_loadenv(env);
	if (ft_set_termcap() == -1)
	{
		ft_putstr_fd("Setup Termcap Error\n", 2);
		return (0);
	}
	if (argc == 2)
	{
		ft_source(envvar, argv[1]);
		ft_dictfree(envvar);
		return (0);
	}
	else if (argc > 2)
	{
		ft_putstr_fd("Usage:\n./21sh\nOR\n./12sh [filename]\n", 2);
		return (0);
	}
	ft_source(envvar, NULL);
	g_prompt = "$>";
	ft_shelloop(envvar, ft_getenv(envvar, "SHELL_PROMPT_CMD"));
	ft_dictfree(envvar);
	return (0);
}
