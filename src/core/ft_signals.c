/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_signals.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/18 12:09:26 by abiri             #+#    #+#             */
/*   Updated: 2019/07/18 13:27:37 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "read_line.h"

int		ft_execright(char *path)
{
	if (access(path, F_OK))
		return (0);
	if (access(path, X_OK))
		return (-1);
	return (1);
}

void	ft_printpathstatus(int status, char *execname)
{
	if (status == 0)
		ft_printf("%s: command not found\n", execname);
	else if (status == -1)
		ft_printf("21sh: permission denied\n");
}

void	ft_sigint(int i)
{
	if (g_sigintstat != 1)
		ft_printf("\n%*@%s%@ ", clRED, g_prompt, i);
	else
		ft_putchar('\n');
	if (pos1.x != pos1.p && pos1.y != 0)
	{
		pos1.index = 0;
		pos1.x = pos1.p;
		pos1.y = 0;
		pos1.num_col = ft_get_size_windz();
		free(pos1.cmd);
		pos1.cmd = NULL;
		pos1.end = ft_memalloc(sizeof(int) * 20);
		free(*pos1.s);
		*pos1.s = ft_strdup("\0");
		ft_mmmm(&pos1.end);
	}
}

void	ft_sigtstp(int i)
{
	ft_printf("\n%*@%s%@ ", cYELLOW, g_prompt, i);
}

void	ft_setsignals(void)
{
	signal(SIGWINCH, ft_win_change);
	signal(SIGINT, ft_sigint);
	signal(SIGTSTP, ft_sigtstp);
}
