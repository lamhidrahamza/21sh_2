/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtin_extended.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/20 21:54:16 by abiri             #+#    #+#             */
/*   Updated: 2019/03/13 18:14:08 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
**	the alias function here uses the same structure as environment variables
**	for the sake of code optimisation
*/

/*
**	ft_joinargs:
**	-	this function joins the command args with the arguments
**	specified in the alias
*/

char	**ft_joinargs(char **new, char **old)
{
	int		len[2];
	int		index;
	int		i;
	char	**result;

	len[0] = ft_tabsize(new);
	len[1] = ft_tabsize(old);
	result = ft_memalloc((len[1] + len[0]) * sizeof(char *));
	i = 0;
	index = -1;
	while (new[++index])
		result[i++] = ft_strdup(new[index]);
	index = 0;
	while (old[++index])
		result[i++] = ft_strdup(old[index]);
	ft_tabfree(old);
	ft_tabfree(new);
	return (result);
}

/*
**	ft_listaliases:
**	-	this function lists the aliases that are set
*/

int		ft_listaliases(t_dict *aliases)
{
	t_entry	*alias;

	alias = aliases->first;
	if (!alias)
		ft_printf("alias: no aliases set\n");
	while (alias)
	{
		ft_printf("%s='%s'\n", alias->key, alias->value);
		alias = alias->next;
	}
	return (1);
}

/*
**	ft_builtin_alias:
**	-	this builtin shell function sets or shows an alias
*/

int		ft_builtin_alias(char **args, t_dict *aliases)
{
	int				count;
	char			**table;

	if (ft_builtin_error(args, "alias", 1, 2))
		return (0);
	if (!args[1])
		ft_listaliases(aliases);
	else if ((count = ft_countchar(args[1], '=')) == 1 && !args[2])
	{
		table = ft_strsplit(args[1], '=');
		if (table[0] && table[1])
			ft_setenv(aliases, table[0], table[1]);
		else
			ft_printf("alias: Incomplete syntax\n");
		ft_tabfree(table);
	}
	else if (count == 0 && args[1] && !args[2])
		ft_printf("%s='%s'\n", args[1], ft_getenv(aliases, args[1]));
	return (1);
}

/*
**	ft_builtin_unlias:
**	-	this builtin function unsets an alias
*/

int		ft_builtin_unalias(char **args, t_dict *aliases)
{
	if (ft_builtin_error(args, "unalias", 2, 2))
		return (0);
	else if (args[1])
		ft_unsetenv(aliases, args[1]);
	return (1);
}

/*
**	ft_getalias:
**	-	this function is responsible for calling the function alias or unalias
**	and then replacing the result in the args array
*/

char	**ft_getalias(char **args)
{
	static t_dict	aliases;
	char			**newargs;
	char			*newcommand;

	if (!args[0])
		return (args);
	if (ft_strequ(args[0], "alias"))
	{
		ft_builtin_alias(args, &aliases);
		ft_tabfree(args);
		return (NULL);
	}
	else if (ft_strequ(args[0], "unalias"))
	{
		ft_builtin_unalias(args, &aliases);
		ft_tabfree(args);
		return (NULL);
	}
	else if ((newcommand = ft_getenv(&aliases, args[0])))
		return (newargs = ft_joinargs(ft_getargs(newcommand), args));
	return (args);
}
