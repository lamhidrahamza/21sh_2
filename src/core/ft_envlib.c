/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_envlib.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/17 14:23:35 by abiri             #+#    #+#             */
/*   Updated: 2019/07/16 12:11:35 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
**	this file contains functions used to manipulate environment variables
**	all these functions were tested and proved to be working
*/

char		**ft_getenvtab(t_dict *envlist, char *execpath)
{
	char	**table;
	size_t	size;
	size_t	counter;
	t_entry	*cursor;

	size = envlist->size;
	cursor = envlist->first;
	counter = 0;
	if (!(table = malloc(sizeof(char *) * (envlist->size + 1))))
		return (NULL);
	while (size)
	{
		if (ft_strequ(cursor->key, "_"))
			table[counter] = ft_strcjoin(cursor->key, '=', execpath);
		else
			table[counter] = ft_strcjoin(cursor->key, '=', cursor->value);
		size--;
		counter++;
		cursor = cursor->next;
	}
	table[counter] = NULL;
	return (table);
}

t_entry		*ft_findenv(t_dict *envlist, char *key)
{
	t_entry *cursor;

	if (!envlist)
		return (NULL);
	cursor = envlist->first;
	while (cursor)
	{
		if (ft_strequ(cursor->key, key))
			return (cursor);
		cursor = cursor->next;
	}
	return (NULL);
}

int		ft_env(t_dict *envlist)
{
	t_entry *env;

	env = envlist->first;
	while(env)
	{
		ft_printf("%s=%s\n", env->key, env->value);
		env = env->next;
	}
	return (1);
}

void		ft_setenv(t_dict *envlist, char *key, char *value)
{
	t_entry *result;

	if (!value)
		return ;
	if (!(result = ft_findenv(envlist, key)))
	{
		ft_addenv(ft_newenv(key, value), envlist);
		return ;
	}
	free(result->value);
	result->value = ft_strdup(value);
}

static void	ft_delfinalenv(t_dict *envlist)
{
	t_entry	*entry;

	if (envlist->first == envlist->last)
	{
		entry = envlist->first;
		envlist->first = NULL;
		envlist->last = NULL;
		ft_envfree(entry);
		envlist->size--;
		return ;
	}
	entry = envlist->first;
	while (entry && entry->next != envlist->last)
		entry = entry->next;
	if (entry->next == envlist->last)
	{
		envlist->last = entry;
		ft_envfree(entry->next);
		entry->next = NULL;
		envlist->size--;
	}
}

void		ft_unsetenv(t_dict *envlist, char *key)
{
	t_entry *result;
	t_entry *next;

	if (!(result = envlist->first) || !(envlist->last))
		return ;
	if (ft_strequ(envlist->last->key, key))
		return (ft_delfinalenv(envlist));
	if (ft_strequ(result->key, key))
	{
		envlist->first = result->next;
		envlist->size--;
		return (ft_envfree(result));
	}
	while (result->next)
	{
		if (ft_strequ(result->next->key, key))
		{
			next = result->next->next;
			ft_envfree(result->next);
			result->next = next;
			envlist->size--;
			break ;
		}
		result = result->next;
	}
}
