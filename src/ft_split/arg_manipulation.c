/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_arg_manipulation.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/17 22:14:38 by abiri             #+#    #+#             */
/*   Updated: 2019/03/26 17:42:31 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "arg_manipulation.h"

/*
**	the ft_new_arg function creates a new arg struct and returns it
**		if the arg is empty, it ignores it and returns NULL
*/

t_arg	*ft_new_arg(char *content, int escape)
{
	t_arg	*result;

	if (!content[0])
	{
		free(content);
		return (NULL);
	}
	if (!(result = malloc(sizeof(t_arg))))
		return (NULL);
	result->content = content;
	result->type = escape;
	result->next = NULL;
	return (result);
}

/*
**	this function adds a new element to the list of arguments
**		if the new element is null it does not add it
**		it the head is not set it adds the next element as a head
*/

void	ft_add_arg(t_arg **head, t_arg *new)
{
	t_arg *cursor;

	if (!head)
		return ;
	if (!new)
		return ;
	if (!*head)
	{
		*head = new;
		return ;
	}
	cursor = *head;
	while (cursor->next)
		cursor = cursor->next;
	cursor->next = new;
}
