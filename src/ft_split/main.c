/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@pm.me>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/13 22:53:30 by abiri             #+#    #+#             */
/*   Updated: 2019/03/23 19:30:23 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arg_manipulation.h"
#include "ft_split_args.h"
#include "libft.h"

int main(void)
{
	char *line;
	t_arg *args;

	get_next_line(0, &line);
	args = ft_splitargs(line);
	while (args)
	{
		ft_printf("|%s|\n", args->content);
		args = args->next;
	}
	return (0);
}
