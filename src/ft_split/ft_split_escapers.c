/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_escapers.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/21 22:12:35 by abiri             #+#    #+#             */
/*   Updated: 2019/03/25 18:36:33 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_split_args.h"

/*
**	this function tells if a \ is an escaping backslash
*/

int		ft_isbackslash(char *str, int index)
{
	if (str[index] == '\\' && str[index + 1] != '\\')
		return (0);
	return (1);
}

/*
**	this function checks if the current character is a
**		non-escaped character from the charset specified
*/

int		ft_fromcharset(char *str, int index, char *charset)
{
	int escapecounter;

	escapecounter = 0;
	if (index == 0 && ft_ischarin(charset, str[0]))
		return (1);
	if (ft_ischarin(charset, str[index]))
	{
		while (--index >= 0 && str[index] == '\\')
			escapecounter++;
		if (escapecounter % 2 == 1)
			return (0);
		return (1);
	}
	return (0);
}

/*
**	this function extracts the redirection and returns it
*/

char	*ft_get_redirection(char *line, int index, int *before, int startindex)
{
	int len;
	int first;

	len = 0;
	first = startindex;
	while (ft_isdigit(line[startindex]) && ++startindex)
		len++;
	*before = startindex - first;
	if (startindex != index)
		return (NULL);
	if ((line[index] == '>' || line[index] == '<') && (++len))
	{
		if (line[index] == line[index + 1] && (++len))
			index++;
		if (line[++index] == '&')
		{
			len++;
			if (line[index + 1] == '-')
				len++;
			else
				while (ft_isdigit(line[++index]))
					len++;
		}
	}
	return (ft_strsub(line, first, len));
}

/*
**	as a solution to get vip chars into separate argument
**		like | < > this function will make a string only composed of
**		one character
*/

char	*ft_vip_args(char *arg, int index, int *offset)
{
	char	*result;

	result = ft_memalloc(2);
	ft_strncpy(result, arg + index, 1);
	*offset = 0;
	return (result);
}
