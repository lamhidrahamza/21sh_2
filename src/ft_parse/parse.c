/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 01:20:11 by abiri             #+#    #+#             */
/*   Updated: 2019/07/18 13:06:56 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "arg_manipulation.h"
#include "ft_parse.h"

int		ft_unescaped_charset(char *str, char *tofind)
{
	int		i;

	i = 0;
	while (str[i])
	{
		if (ft_fromcharset(str, i, tofind))
			return (1);
		i++;
	}
	return (0);
}

int		ft_is_redirectin(t_arg *part)
{
	char	*str;
	int		len;

	str = part->content;
	len = ft_strlen(str);
	if (ft_match(str, "%d<|<") == len)
	{
		part->type = TYPE_REDIRECT_IN;
		return (TYPE_FILEREAD);
	}
	else if (ft_match(str, "%d<<|<<") == len)
	{
		part->type = TYPE_REDIRECT_IN;
		return (TYPE_HEREDOC);
	}
	else if (ft_match(str, "(%d<<|%d<)(&-|&%d|%0)") == len)
	{
		part->type = TYPE_REDIRECT_IN;
		return (TYPE_COMMAND | TYPE_PIPE | TYPE_NONE | TYPE_REDIRECT_IN |
				TYPE_REDIRECT_OUT | TYPE_SEMICOLON);
	}
	return (0);

}

int		ft_is_fileread(t_arg *part)
{
	if (ft_unescaped_charset((part->content), VIP_CHARS))
		return (0);
	if (!access(part->content, F_OK) && !access(part->content, R_OK))
	{
		part->type = TYPE_FILEREAD;
		return (TYPE_COMMAND | TYPE_PIPE | TYPE_NONE | TYPE_REDIRECT_IN |
				TYPE_REDIRECT_OUT | TYPE_SEMICOLON);
	}
	return (FILE_ERROR);
}

int		ft_is_redirectout(t_arg *part)
{
	char	*str;
	int		len;

	str = part->content;
	len = ft_strlen(str);
	if (ft_match(str, "%d>>|%d>|>>|>") == len)
	{
		part->type = TYPE_REDIRECT_OUT;
		return (TYPE_FILEWRITE);
	}
	else if (ft_match(str, "(%d>>|%d>)(%0|&-|&%d)") == len)
	{
		part->type = TYPE_REDIRECT_OUT;
		return (TYPE_COMMAND | TYPE_PIPE | TYPE_NONE | TYPE_REDIRECT_IN |
				TYPE_REDIRECT_OUT | TYPE_SEMICOLON);
	}
	return (0);
}

int		ft_is_filewrite(t_arg *part)
{
	int fd;

	fd = open(part->content, O_WRONLY | O_CREAT, 0644);
	if (ft_unescaped_charset((part->content), VIP_CHARS))
		return (0);
	part->type = TYPE_FILEWRITE;
	if (fd < 0)
		return (FILE_ERROR);
	close(fd);
	return (TYPE_COMMAND | TYPE_PIPE | TYPE_NONE | TYPE_REDIRECT_IN |
			TYPE_REDIRECT_OUT | TYPE_SEMICOLON);
}

int		ft_is_pipe(t_arg *part)
{
	if (ft_strequ(part->content, "|"))
	{
		part->type = TYPE_PIPE;
		return (TYPE_COMMAND | TYPE_REDIRECT_OUT);
	}
	return (0);
}

int		ft_is_command(t_arg *part)
{
	if (ft_is_pipe(part) || ft_is_redirectout(part) || ft_is_redirectin(part))
		return (0);
	if (ft_unescaped_charset((part->content), VIP_CHARS))
		return (0);
	part->type = TYPE_COMMAND;
	return (TYPE_COMMAND | TYPE_PIPE | TYPE_REDIRECT_OUT | TYPE_REDIRECT_IN
		   | TYPE_NONE | TYPE_SEMICOLON);
}

int		ft_is_heredoc(t_arg *part)
{
	part->type = TYPE_FILEWRITE;
	return (TYPE_COMMAND | TYPE_PIPE | TYPE_NONE | TYPE_REDIRECT_IN |
				TYPE_REDIRECT_OUT | TYPE_SEMICOLON);
}

int		ft_is_semicolon(t_arg *part)
{
	if (!ft_strequ(part->content, ";"))
		return (0);
	part->type = TYPE_SEMICOLON;
	return (TYPE_COMMAND | TYPE_REDIRECT_IN | TYPE_REDIRECT_OUT | TYPE_NONE
			| TYPE_SEMICOLON);
}

/*
**	this struct table contains the functions handeling each syntax element
**	in an easy way that will make it easy to access each une using a hash
**	function
*/

const	t_syntax_element	syntaxtab[] = {
		{.type = TYPE_SEMICOLON, .func = &ft_is_semicolon},
		{.type = TYPE_REDIRECT_IN, .func = &ft_is_redirectin},
		{.type = TYPE_PIPE, .func = &ft_is_pipe},
		{.type = TYPE_REDIRECT_OUT, .func = &ft_is_redirectout},
		{.type = TYPE_FILEREAD, .func = &ft_is_fileread},
		{.type = TYPE_FILEWRITE, .func = &ft_is_filewrite},
		{.type = TYPE_HEREDOC, .func = &ft_is_heredoc},
		{.type = TYPE_COMMAND, .func = &ft_is_command},
		{.type = TYPE_NONE, .func = NULL},
		{.type = 0, .func = NULL}
		};

/*
**	this function is a hash function given the type will get the index
*/

int		ft_syntax_hashfunc(int type)
{
	int index;

	index = 0;
	while (index < 7)
	{
		if (type & (1 << index))
			return (index);
		index++;
	}
	return (7);
}

int		ft_verify_type(t_arg *part, int	typein)
{
	int key;
	t_syntax_verif *myfunc;
	int result;

	int i = -1;
	while (++i < 9)
	{
		key = (1 << i);
		if (!(key & typein))
			continue ;
		myfunc = syntaxtab[ft_syntax_hashfunc(key)].func;
		if (myfunc && (result = myfunc(part)))
			return (result);
	}
	return (0);
}

int		ft_parse_args(t_arg *parts)
{
	int types;

	types = TYPE_REDIRECT_IN | TYPE_COMMAND | TYPE_REDIRECT_OUT;
	while(parts)
	{
		if (!(types = ft_verify_type(parts, types)))
		{
			ft_printf("21sh: parse error near: `%s'\n", parts->content);
			return (PARSE_ERROR);
		}
		else if (types == FILE_ERROR)
		{
			ft_printf("21sh: permission denied on `%s'\n", parts->content);
			return (PARSE_ERROR);
		}
		parts = parts->next;
	}
	if (!(types & TYPE_NONE))
		return (PARSE_INCOMPLETE);
	return (PARSE_OK);
}

/*
int main(void)
{
	char *line;
	int status;
	t_arg	*arglist;

	get_next_line(0, &line);
	arglist = ft_splitargs(line, &status);
	ft_printf("parse status is : %d\n", status);
	ft_parse_args(arglist);
	while (arglist)
	{
		ft_printf("|%s| is a : ", arglist->content);
		if (arglist->type == TYPE_REDIRECT_IN)
			ft_printf("%*@redirection in%@\n", cGREEN);
		else if (arglist->type == TYPE_COMMAND)
			ft_printf("%*@command or argument%@\n", cGREEN);
		else if (arglist->type == TYPE_PIPE)
			ft_printf("%*@pipe%@\n", cGREEN);
		else if (arglist->type == TYPE_REDIRECT_OUT)
			ft_printf("%*@redirect out%@\n", cGREEN);
		else if (arglist->type == TYPE_FILEREAD)
			ft_printf("%*@readable file%@\n", cGREEN);
		else if (arglist->type == TYPE_FILEWRITE)
			ft_printf("%*@writable file%@\n", cGREEN);
		else if (arglist->type == TYPE_SEMICOLON)
			ft_printf("%*@semicolon%@\n", cGREEN);
		else if (arglist->type == 0)
			ft_printf("%*@ERROR%@\n", cRED);
		arglist = arglist->next;
	}
}*/
