project by:
	- KernelOverseer (Aymane Biri)







                                               
                                               
				      ,----,       ,---,             ,---,     
				    .'   .' \   ,`--.' |           ,--.' |     
				  ,----,'    | /    /  :           |  |  :     
				  |    :  .  ;:    |.' '  .--.--.  :  :  :     
				  ;    |.'  / `----':  | /  /    ' :  |  |,--. 
				  `----'/  ;     '   ' ;|  :  /`./ |  :  '   | 
				    /  ;  /      |   | ||  :  ;_   |  |   /' : 
				   ;  /  /-,     '   : ; \  \    `.'  :  | | | 
				  /  /  /.`|     |   | '  `----.   \  |  ' | : 
				./__;      :     '   : | /  /`--'  /  :  :_:,' 
				|   :    .'      ;   |.''--'.     /|  | ,'     
				;   | .'         '---'    `--'---' `--''       
				`---'                                          
					╔╦╗┌─┐┌─┐┬ ┬┌┬┐┌─┐┌┐┌┌┬┐┌─┐┌┬┐┬┌─┐┌┐┌
==================== ║║│ ││  │ ││││├┤ │││ │ ├─┤ │ ││ ││││=====================
					═╩╝└─┘└─┘└─┘┴ ┴└─┘┘└┘ ┴ ┴ ┴ ┴ ┴└─┘┘└┘





			   CURRENT PROBLEMS ARE DESCRIBED HERE TEMPORARILY

		- Variables should still be treated, it will be very easy just need
							  to not forget them

		- signals should be manipulates especially the Ctrl-C when using
							the line editing utility









   ┌─┐┌─┐┌┐┌┌─┐┬─┐┌─┐┬    ┌─┐┬─┐┌─┐┌─┐┬─┐┌─┐┌┬┐  ┌─┐┌┬┐┬─┐┬ ┬┌─┐┌┬┐┬ ┬┬─┐┌─┐
:::│ ┬├┤ │││├┤ ├┬┘├─┤│    ├─┘├┬┘│ ││ ┬├┬┘├─┤│││  └─┐ │ ├┬┘│ ││   │ │ │├┬┘├┤ :::
   └─┘└─┘┘└┘└─┘┴└─┴ ┴┴─┘  ┴  ┴└─└─┘└─┘┴└─┴ ┴┴ ┴  └─┘ ┴ ┴└─└─┘└─┘ ┴ └─┘┴└─└─┘


	- 21sh is based on Minishell, what is added is an in-line editing
  capability to make the user capable of editing the line without the need
						of deleting it entirely.

	- Another new capability is the capability of piping input and output
 and using redirections like writing output to file or reading input from file

	- the idea now is to make a linear structure that will describe the flow
 				  of the program during all of it's steps



	Line editing -> Spliting input -> replacing variables and paths -> Parsing
		-> linking inputs and outputs and interpreting the command


				The steps will be all explained as follows :


						╦  ┬┌┐┌┌─┐  ╔═╗┌┬┐┬┌┬┐┬┌┐┌┌─┐
::::::::::::			║  ││││├┤   ║╣  │││ │ │││││ ┬              ::::::::::::
						╩═╝┴┘└┘└─┘  ╚═╝─┴┘┴ ┴ ┴┘└┘└─┘











					  ╔═╗┌─┐┬  ┬┌┬┐┬┌┐┌┌─┐  ╦┌┐┌┌─┐┬ ┬┌┬┐
:::::::::::::		  ╚═╗├─┘│  │ │ │││││ ┬  ║│││├─┘│ │ │		   :::::::::::: 
					  ╚═╝┴  ┴─┘┴ ┴ ┴┘└┘└─┘  ╩┘└┘┴  └─┘ ┴ 



				spliting input will be on multiple stages
				I figured out a very good way of doing it

		- first i made some functions and defines to verify if a char
						  is a quote or a separarator
				defined all separators and whitespaces as:

				
					# define SPLITCHARS "\t\n\v\f\r "
					# define VIP_CHARS "|<>"


				then made a function called ft_fromcharset()
				this function gets a charset and a string and
						index of element to be tested
				  and sees if the element is from the charset
							and not escaped by a \
					then added some defines for them		


			# define ISSEP(s, i) ft_fromcharset(s, i, SPLITCHARS)
			# define ISQUOT(s, i) ft_fromcharset(s, i, "'\"")
			# define ISVIP(s, i) ft_fromcharset(s, i, VIP_CHARS)

			
		  - then i look thru all characters , if i find a quote
	    i save the index as a start, and ignore all split characters
			 inside the quotesm until i find a similar quote

		   - then use the function ft_getarglen() that will count
		 the exact size of the string necessary to hold the argument
		 	  without the quotes and the escaping characters

			- next step is allocating the size and copying all the
		necessary argument characters without the escaping characters

			- the next step now is to manage the VIP_CHARS
		  like | < > so now it takes them as a sepparate argument

		   a struct s_arg with typedef t_arg will act as a linked list
						with the splitted arguments
		

			   |===============================================|
			   |											   |
			   |											   |
			   |											   |
			   |			typedef struct	s_arg			   |
			   |			{								   |
			   |				char			*content;	   |
			   |				int				type;		   |
			   |				struct s_arg	*next;		   |
			   |			}				t_arg;			   |
			   |											   |
			   |											   |
			   |_______________________________________________|


		 The linked list will be now passed to be evaluated and parsed
		 	  the type variable of the struct will contain an int
			   whose bits present a type of command line element
						like redirections and filenames

		
		⚠	a problem with arguments spliting is knowing when a character
		is representing itself or escaped since the quotes and backslash
								is removed.
		  the solution now is to escape special chars with a backslash
			 until it is passed thru parsing, then recovering it.



							 ╔═╗┌─┐┬─┐┌─┐┬┌┐┌┌─┐
::::::::::::::				 ╠═╝├─┤├┬┘└─┐│││││ ┬				   ::::::::::::
							 ╩  ┴ ┴┴└─└─┘┴┘└┘└─┘



			The operation of parsing relays on an abstract syntax tree
					the rules it has can be represented as:


			   the parsing will begin by verifying the types of all
								input components
					every line shall begin with : TYPE_COMMAND
							or TYPE_REDIRECT_IN




				TYPE_REDIRECT_IN   ───> 	FILEREAD

				TYPE_FILEREAD 	   ───> 	TYPE_COMMAND

				TYPE_COMMAND	   ─┬─>		TYPE_PIPE
									├─>		TYPE_ARGUMENT
									├─>		TYPE_REDIRECT_OUT
									└─>		TYPE_NONE
				
				TYPE_REDIRECT_OUT  ───>		TYPE_FILEWRITE

				TYPE_FILEWRITE	   ─┬─>		TYPE_ARGUMENT
									├─>		TYPE_NONE
									├─>		TYPE_PIPE
									└─>		TYPE_REDIRECT_OUT
	


				 these typedefs can be found in the ft_types.h

				  the t_arg linked list obtained from the last
			  spliting function will now be filled with more data
			as the types of each arfument eill be filled in this step

	another way of communicating parse errors and other no such file errors
		between functions is another set of defines, setting upper bits
	    of the type int, and a mask TYPE_ERROR_MASK allowing to see them

					then all the data parsed will be saved
				as a linked list, the interpreted in the next step


			 ╦  ╦┌─┐┬─┐┬┌─┐┌┐ ┬  ┌─┐┌─┐  ┌─┐┌┐┌┌┬┐  ╔═╗┌─┐┌┬┐┬ ┬┌─┐
:::::::::::::╚╗╔╝├─┤├┬┘│├─┤├┴┐│  ├┤ └─┐  ├─┤│││ ││  ╠═╝├─┤ │ ├─┤└─┐::::::::::::
			  ╚╝ ┴ ┴┴└─┴┴ ┴└─┘┴─┘└─┘└─┘  ┴ ┴┘└┘─┴┘  ╩  ┴ ┴ ┴ ┴ ┴└─┘

						╦┌┐┌┌┬┐┌─┐┬─┐┌─┐┬─┐┌─┐┌┬┐┬┌┐┌┌─┐
::::::::::::::			║│││ │ ├┤ ├┬┘├─┘├┬┘├┤  │ │││││ ┬		   ::::::::::::
						╩┘└┘ ┴ └─┘┴└─┴  ┴└─└─┘ ┴ ┴┘└┘└─┘
