/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_prepare.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/22 19:51:50 by abiri             #+#    #+#             */
/*   Updated: 2019/03/12 21:59:26 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PREPARE_H
# define FT_PREPARE_H
# define NO_FD -1
# define REDIR_IN 0
# define REDIR_OUT 1

/*
**	this struct will be holding redirection to file or another fd
*/

typedef struct	s_redirect
{
	int					fdfrom;
	int					fdto;
	char				direction;
	char				*filename;
	struct s_redirect	*next;
}				t_redirect;

/*
**	in here i will define the struct that will hold information
**		about the current command 
*/

typedef struct	s_command
{
	char				**argv;
	t_redirect			*redirections;
	t_redirect			*lastredirection;
	struct	s_command	*next;
}				t_command;

/*
**	this struct will hold informations about the command line
**		and will also contain a list of commands
*/

typedef	struct	s_interpret
{
	t_command	*firstcmd;
	t_command	*lastcmd;
}				t_interpret;

#endif
