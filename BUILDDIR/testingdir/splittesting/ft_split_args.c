/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/17 20:50:15 by abiri             #+#    #+#             */
/*   Updated: 2019/03/23 19:05:43 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "arg_manipulation.h"
#include "ft_split_macros.h"
#include "ft_split_escapers.h"
#include "ft_split_args.h"

/*
**	this function will calculate the size of each argument
**		ignoring all escapes, this size will be considered as
**		final size
*/

int		ft_getarglen(char *arg, int start, int length)
{
	int		i;
	int		len;
	char	quote;

	i = start;
	len = 0;
	quote = 0;
	while (length-- && ++i)
	{
		if (!INQUOTE && ISQUOT(arg, i))
		{
			SETINQUOTE;
			SETQUOTE(arg[i]);
		}
		else if (INQUOTE && ISQUOT(arg, i))
		{
			UNSETINQUOTE;
			SETQUOTE(0);
		}
		else if (ISVIP(arg, i))
			len += 2;
		else
			len++;
	}
	return (len);
}

/*
**	after you decide when to split every argument this function will
**		process every argument and remove unnecessary quotes
*/

char	*ft_get_args(char *arg, int start, int len)
{
	char	quote;
	char	*result;
	int		j;

	j = 0;
	quote = 0;
	if (start == INDEX_NONE)
		return (NULL);
	result = ft_memalloc(ft_getarglen(arg, start, len) + 1);
	while (len)
	{
		if (!INQUOTE && ISQUOT(arg, start) && (SETINQUOTE))
			SETQUOTE(arg[start]);
		else if (INQUOTE && ISQUOT(arg, start) && arg[start] == LASTQUOTE
				&& (UNSETINQUOTE))
			SETQUOTE(0);
		else
		{
			result[j++] = (ISVIP(arg, start)) ? '\\' : arg[start];
			result[j++] = (ISVIP(arg, start)) ? arg[start] : result[--j];
		}
		start = start + 1 + 0 * len--;
	}
	return (result);
}

/*
**	this function will add a new arg to the list with the splitted text
*/

int		ft_add_new_arg(t_arg **list, char *line, int start, int end)
{
	t_arg	*newarg;

	newarg = ft_new_arg(ft_get_args(line, start, end), 0);
	ft_add_arg(list, newarg);
	return (1);
}

/*
**	this function will add some special chars to the argument list
**		as they are called VIPCHARS
*/

int		ft_vipchars(char *line, int index, t_arg **list, int startindex)
{
	int		before;
	char	*result;

	if (line[index] == '>' || line[index] == '<')
	{
		if (!(result = ft_get_redirection(line, index, &before, startindex)))
			result = ft_vip_args(line, index, &before);
	}
	else
		result = ft_vip_args(line, index, &before);
	if (startindex != index - before)
		ft_add_new_arg(list, line, startindex, before);
	ft_add_arg(list, ft_new_arg(result, 0));
	return (ft_strlen(result) - before - 1);
}

/*
**	this function will know when and where to split every arg
**		depending on escaping and other factors
*/

t_arg	*ft_splitargs(char *line)
{
	int		i;
	int		startindex;
	char	quote;
	t_arg	*result;

	i = -1;
	quote = 0;
	startindex = 0;
	result = NULL;
	while (++i == 0 || line[i - 1])
	{
		if (ISQUOT(line, i) && !INQUOTE && (SETINQUOTE))
			SETQUOTE(line[i]);
		else if (ISQUOT(line, i) && line[i] == LASTQUOTE && (UNSETINQUOTE))
			SETQUOTE(0);
		else if (!INQUOTE && startindex == INDEX_NONE && ISSEP(line, i))
			startindex = i + 1;
		else if (!INQUOTE && ISSEP(line, i) && ft_add_new_arg(&result, line,
					startindex, i - startindex))
			startindex = i + 1;
		else if (!INQUOTE && ISVIP(line, i))
			startindex = i + 1 + ft_vipchars(line, i, &result, startindex);
	}
	return (result);
}
