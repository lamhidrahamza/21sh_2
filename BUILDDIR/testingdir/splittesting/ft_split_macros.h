/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_macros.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/21 22:38:46 by abiri             #+#    #+#             */
/*   Updated: 2019/02/25 21:19:03 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SPLIT_MACROS_H
# define FT_SPLIT_MACROS_H
# include "libft.h"

/*
**	In here i deined some macros to symplify working quotes and spliting
**		characters.
*/

# define SPLITCHARS "\t\n\v\f\r "
# define VIP_CHARS "|<>;"
# define INDEX_NONE -1

/*
**	Some inline functions
*/

# define ISSEP(s, i) (ft_fromcharset(s, i, SPLITCHARS) || !s[i])
# define ISQUOT(s, i) ft_fromcharset(s, i, "'\"")
# define ISVIP(s, i) ft_fromcharset(s, i, VIP_CHARS)

/*
**	To make working with quotes efficient, two variables (a char and a boolean)
**		can be merged into only, where one bit can signify if we are inside
**		or outside quotes, the other seven bits can encode the last quote
**	These defines help manipulate the variable
*/

# define INQUOTE	(quote & 128)
# define LASTQUOTE (quote & 127)
# define SETQUOTE(c) quote = (quote & 128) | c
# define SETINQUOTE quote |= 128
# define UNSETINQUOTE quote &= 127

#endif
