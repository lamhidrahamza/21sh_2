/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/25 17:09:18 by abiri             #+#    #+#             */
/*   Updated: 2019/02/25 17:15:41 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_split_args.h"
#include "arg_manipulation.h"
#include "libft.h"

int main(void)
{
	char *line;
	t_arg *args;

	get_next_line(0, &line);
	args = ft_splitargs(line);
	while (args)
	{
		ft_printf("|%s|\n", args->content);
		args = args->next;
	}
	return (0);
}
