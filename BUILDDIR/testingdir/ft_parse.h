/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/05 23:49:51 by abiri             #+#    #+#             */
/*   Updated: 2019/03/11 14:05:19 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PARSE_H
# define FT_PARSE_H
# define TYPE_REDIRECT_IN 1
# define TYPE_PIPE 2
# define TYPE_REDIRECT_OUT 4
# define TYPE_FILEREAD 8
# define TYPE_FILEWRITE 16
# define TYPE_COMMAND 32
# define TYPE_NONE 64
# define TYPE_ERROR_MASK 2147483520
# define TYPE_NO_FILE_ERROR 128
# define TYPE_LOGIC_ERROR 256

typedef	int 	(t_syntax_verif)(t_arg *);

typedef struct	s_syntax_element
{
	int				type;
	t_syntax_verif	*func;
}				t_syntax_element;

void	ft_parse_args(t_arg *parts);

#endif
