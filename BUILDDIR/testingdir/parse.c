/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 01:20:11 by abiri             #+#    #+#             */
/*   Updated: 2019/03/20 23:16:26 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "arg_manipulation.h"
#include "ft_split_args.h"
#include "ft_parse.h"
#include "ft_match.h"

int		ft_is_redirectin(t_arg *part)
{
	char	*str;
	int		len;

	str = part->content;
	len = ft_strlen(str);
	if (ft_match_expressions(str, "%d<|%d<<|<|<<") == len)
	{
		part->type = TYPE_REDIRECT_IN;
		return (TYPE_FILEREAD);
	}
	else if (ft_match_expressions(str, "(%d<|%d<<)(&-|&%d|%0)") == len)
	{
		part->type = TYPE_REDIRECT_IN;
		return (TYPE_COMMAND | TYPE_PIPE | TYPE_NONE | TYPE_REDIRECT_IN |
				TYPE_REDIRECT_OUT);
	}
	return (0);
}

int		ft_is_fileread(t_arg *part)
{
	if (!access(part->content, F_OK) && !access(part->content, R_OK))
	{
		part->type = TYPE_FILEREAD;
		return (TYPE_COMMAND | TYPE_PIPE | TYPE_NONE);
	}
	return (TYPE_NO_FILE_ERROR);
}

int		ft_is_redirectout(t_arg *part)
{
	char	*str;
	int		len;

	str = part->content;
	len = ft_strlen(str);
	if (ft_match_expressions(str, "%d>|%d>>|>|>>") == len)
	{
		part->type = TYPE_REDIRECT_OUT;
		return (TYPE_FILEWRITE);
	}
	else if (ft_match_expressions(str, "(%d>|%d>>)(&-|&%d|%0)") == len)
	{
		part->type = TYPE_REDIRECT_OUT;
		return (TYPE_COMMAND | TYPE_PIPE | TYPE_NONE | TYPE_REDIRECT_IN |
				TYPE_REDIRECT_OUT);
	}
	return (0);
}

int		ft_is_filewrite(t_arg *part)
{
	part->type = TYPE_FILEWRITE;
	return (TYPE_COMMAND | TYPE_PIPE | TYPE_NONE);
}

int		ft_is_pipe(t_arg *part)
{
	if (ft_strequ(part->content, "|"))
	{
		part->type = TYPE_PIPE;
		return (TYPE_COMMAND | TYPE_REDIRECT_OUT | TYPE_REDIRECT_IN);
	}
	return (0);
}

int		ft_is_command(t_arg *part)
{
	part->type = TYPE_COMMAND;
	return (TYPE_COMMAND | TYPE_PIPE | TYPE_REDIRECT_OUT | TYPE_REDIRECT_IN
		   |  TYPE_NONE);
}

/*
**	this struct table contains the functions handeling each syntax element
**	in an easy way that will make it easy to access each une using a hash
**	function
*/

const	t_syntax_element	syntaxtab[] = {
		{.type = TYPE_REDIRECT_IN, .func = &ft_is_redirectin},
		{.type = TYPE_PIPE, .func = &ft_is_pipe},
		{.type = TYPE_REDIRECT_OUT, .func = &ft_is_redirectout},
		{.type = TYPE_FILEREAD, .func = &ft_is_fileread},
		{.type = TYPE_FILEWRITE, .func = &ft_is_filewrite},
		{.type = TYPE_COMMAND, .func = &ft_is_command},
		{.type = TYPE_NONE, .func = NULL},
		{.type = 0, .func = NULL}
		};

/*
**	this function is a hash function given the type will get the index
*/

int		ft_syntax_hashfunc(int type)
{
	int index;

	index = 0;
	while (index < 7)
	{
		if (type & (1 << index))
			return (index);
		index++;
	}
	return (7);
}

int		ft_verify_type(t_arg *part, int	typein)
{
	int key;
	t_syntax_verif *myfunc;
	int result;

	int i = -1;
//	ft_printf("now arg |%s|\n", part->content);
	while (++i < 8)
	{
		key = (1 << i);
		if (!(key & typein))
			continue ;
//		ft_printf("checking for : %d\n", key);
		myfunc = syntaxtab[ft_syntax_hashfunc(key)].func;
		if (myfunc && ((result = myfunc(part)) & ~TYPE_ERROR_MASK))
			return (result);
	}
	return (0);
}

void	ft_parse_args(t_arg *parts)
{
	int types;

	types = TYPE_REDIRECT_IN | TYPE_COMMAND | TYPE_REDIRECT_OUT;
	while(parts)
	{
		if (!(types = ft_verify_type(parts, types)))
		{
			ft_printf("parse error near: `%s'\n", parts->content);
			return ;
		}
		parts = parts->next;
	}
	if (!(types & TYPE_NONE))
		ft_printf("incomplete command\n");
}

/*
int main(void)
{
	char *line;
	t_arg	*arglist;

	get_next_line(0, &line);
	arglist = ft_splitargs(line);
	ft_parse_args(arglist);
	while (arglist)
	{
		ft_printf("|%s| is a : ", arglist->content);
		if (arglist->type == TYPE_REDIRECT_IN)
			ft_printf("%*@redirection in%@\n", cGREEN);
		else if (arglist->type == TYPE_COMMAND)
			ft_printf("%*@command or argument%@\n", cGREEN);
		else if (arglist->type == TYPE_PIPE)
			ft_printf("%*@pipe%@\n", cGREEN);
		else if (arglist->type == TYPE_REDIRECT_OUT)
			ft_printf("%*@redirect out%@\n", cGREEN);
		else if (arglist->type == TYPE_FILEREAD)
			ft_printf("%*@readable file%@\n", cGREEN);
		else if (arglist->type == TYPE_FILEWRITE)
			ft_printf("%*@writable file%@\n", cGREEN);
		else if (arglist->type == 0)
			ft_printf("%*@ERROR%@\n", cRED);
		arglist = arglist->next;
	}
}
*/
