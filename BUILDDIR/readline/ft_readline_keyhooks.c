/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_readline_keyhooks.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/07 21:43:18 by abiri             #+#    #+#             */
/*   Updated: 2019/02/18 22:21:34 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_readline.h"

void	ft_key_left(t_readline_buffer *buff)
{
	if (buff->index  == 0)
		return ;
	buff->index--;
	ft_apply_cap(ONE_LEFT);
}

void	ft_key_right(t_readline_buffer *buff)
{
	exit(1);
	if (!buff->stock[buff->index])
		return ;
	if (buff->index && !((buff->index + 1) % ft_refreshcap_value(GET_WIDTH)))
		ft_apply_cap(ONE_DOWN);
	else
		ft_apply_cap(ONE_RIGHT);
	buff->index++;
}

void	ft_key_back(t_readline_buffer *buff)
{
	if (buff->index == 0)
		return ;
	ft_readline_charunoffset(buff);
	ft_apply_cap(ONE_LEFT);
}

void	ft_key_default(t_readline_buffer *buff, int c)
{
	if (c > 127 || c < -128)
		return;
	ft_readline_charoffset(buff);
	buff->stock[buff->index++] = c;
	ft_putchar(c);
}
