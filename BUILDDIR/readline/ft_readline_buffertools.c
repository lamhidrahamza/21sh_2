/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_readline_buffertools.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/04 22:20:06 by abiri             #+#    #+#             */
/*   Updated: 2019/02/15 22:53:50 by helmanso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_readline.h"

t_readline_buffer	*ft_readline_newbuffer(void)
{
	t_readline_buffer *result;

	result = ft_memalloc(sizeof(t_readline_buffer));
	result->size = READLINE_BUFFSIZE;
	result->index = 0;
	result->stock = ft_memalloc(READLINE_BUFFSIZE + 1);
	return (result);
}

void				ft_readline_addbuffer(t_readline_buffer *buff)
{
	char	*oldstock;

	oldstock = buff->stock;
	buff->size += READLINE_BUFFSIZE;
	buff->stock = ft_memalloc(buff->size + 1);
	buff->stock = ft_strcpy(buff->stock, oldstock);
	free(oldstock);
}
