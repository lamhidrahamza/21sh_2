/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_readline_manipulation_tools.c                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/07 21:47:22 by abiri             #+#    #+#             */
/*   Updated: 2019/02/07 21:58:23 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_readline.h"

void	ft_readline_charunoffset(t_readline_buffer *buff)
{
	size_t	lentonull;
	size_t	index;

	buff->index--;
	lentonull = ft_strlen(&buff->stock[buff->index + 1]) + 1;
	index = buff->index;
	ft_apply_cap(SAVE_CURSOR);
	ft_apply_cap(ONE_LEFT);
	while (lentonull)
	{
		buff->stock[index] = buff->stock[index + 1];
		ft_putchar(buff->stock[index]);
		index++;
		lentonull--;
	}
	ft_putchar(' ');
	ft_apply_cap(RECOVER_CURSOR);
}

void	ft_readline_charoffset(t_readline_buffer *buff)
{
	size_t	offset;
	size_t	lentonull;
	char	temp;
	char	new;

	offset = buff->index;
	new = buff->stock[offset];
	lentonull = ft_strlen(&(buff->stock[buff->index]));
	if (!lentonull)
		return ;
	if (buff->index + lentonull >= buff->size)
		ft_readline_addbuffer(buff);
	ft_apply_cap(SAVE_CURSOR);
	while (buff->stock[offset])
	{
		temp = buff->stock[offset + 1];
		buff->stock[offset + 1] = new;
		ft_putchar(buff->stock[offset]);
		new = temp;
		offset++;
	}
	ft_apply_cap(RECOVER_CURSOR);
}

