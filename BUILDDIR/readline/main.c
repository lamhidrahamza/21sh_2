/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/04 01:38:12 by abiri             #+#    #+#             */
/*   Updated: 2019/02/18 22:19:35 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft/libft.h"
#include <time.h>
#include "ft_termcaps.h"
#include "ft_readline.h"

void	ft_recoverterm(int mode)
{
	static struct termios	originalops;
	int						res;

	res = 0;
	if (mode == SAVE_TERM)
		res = tcgetattr(STDIN_FILENO, &originalops);
	else if (mode == RECOVER_TERM)
		res = tcsetattr(STDIN_FILENO, TCSANOW, &originalops);
	if (res < 0)
		ft_fatal("Cannot modify terminal state\n");
}

void ft_fatal(char *str)
{
	ft_putstr_fd(str, 2);
	exit(1);
}

t_readline_handler_tab	*ft_loadhooks(void)
{
	t_readline_handler_tab	*final;
	t_readline_handler_tab	readline_keyhook[] = 
	{
		{.key = K_LEFT, .action = &ft_key_left},
		{.key = K_BACK, .action = &ft_key_back},
		{.key = K_RIGHT, .action = &ft_key_right},
		{.key = 0, .action = NULL}
	};

	final = malloc(sizeof(t_readline_handler_tab) * 4);
	ft_memcpy(final, readline_keyhook, sizeof(t_readline_handler) * 4);
	return (final);
}

void	ft_readline_keyhook(t_readline_buffer *buff, int key)
{
	size_t							i;
	static t_readline_handler_tab	*readline_keyhook;

	if (!readline_keyhook)
		readline_keyhook = ft_loadhooks();
	i = 0;
	while (readline_keyhook[i].action)
	{
		if (readline_keyhook[i].key == key)
		{
			readline_keyhook[i].action(buff);
			break ;
		}
		i++;
	}
	if (readline_keyhook[i].action == NULL)
		ft_key_default(buff, key);
}

int		ft_readline(char **line)
{
	int					c;
	t_readline_buffer	*buff;

	buff = ft_readline_newbuffer();
	while ((c = ft_getch()) != K_ENTER)
	{
		if (buff->index >= buff->size)
			ft_readline_addbuffer(buff);
		ft_readline_keyhook(buff, c);
	}
	ft_apply_cap(ONE_DOWN);
	*line = buff->stock;
	return (0);
}
