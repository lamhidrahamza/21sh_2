/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/28 18:47:49 by abiri             #+#    #+#             */
/*   Updated: 2019/02/05 05:04:05 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_readline.h"

void	ft_reset_n_exit(char *str)
{
/*
** this function will have to display error and reset the terminal;
*/
	ft_putstr_fd(str, 2);
	exit(1);
}

int		ft_apply_cap(char *cap)
{
	char *capability;

	if (!(capability = tgetstr(cap, NULL)))
		ft_reset_n_exit("Some required capabilities are not \
supported by your terminal\n");
	tputs(capability, 1, ft_putchar);
	return (0);
}

void	ft_n_cap(char *cap, int times)
{
	if (times < 0)
		return ;
	while (times--)
		ft_apply_cap(cap);
}

int		ft_refreshcap_value(char *cap)
{
	tgetent(getenv("TERM"), 0);
	return (tgetnum(cap));
}
