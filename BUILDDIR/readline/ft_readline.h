/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_readline.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/04 01:51:42 by abiri             #+#    #+#             */
/*   Updated: 2019/02/15 22:44:21 by helmanso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_READLINE_H
# define FT_READLINE_H
# include <term.h>
# include <curses.h>
# include <termios.h>
# include <sys/ioctl.h>
# include <fcntl.h>
# include <signal.h>
# include <stdbool.h>
# include <sys/stat.h>
# include <time.h>
# include "../libft/libft.h"
# include "ft_termcaps.h"
# include "ft_keys.h"
# define SAVE_TERM 0
# define RECOVER_TERM 1
# define READLINE_BUFFSIZE 10

/*
** READLINE FUNCTION PROTOTYPE
*/

int		ft_readline(char **line);

/*
**	typedef for struct holding information about the buffer used for storing
**	and editing characters in readline function
*/

typedef struct	s_readline_buffer
{
		size_t	size;
		size_t	index;
		char	*stock;
}				t_readline_buffer;

/*
**	typedef to pointer to functions used as events for every key
*/

typedef	void	(*t_readline_handler)(t_readline_buffer *);

typedef struct			s_readline_handler_tab
{
	int					key;
	t_readline_handler	action;
}						t_readline_handler_tab;

/*
**	key handling functions
*/

void	ft_key_left(t_readline_buffer *buff);
void	ft_key_right(t_readline_buffer *buff);
void	ft_key_back(t_readline_buffer *buff);
void	ft_key_default(t_readline_buffer *buff, int c);

/*
**	key handling helper functions
*/

void	ft_readline_charunoffset(t_readline_buffer *buff);
void	ft_readline_charoffset(t_readline_buffer *buff);

/*
**	buffer manipulation functions
*/

t_readline_buffer	*ft_readline_newbuffer(void);
void				ft_readline_addbuffer(t_readline_buffer *buff);

/*
**	functions prototypes :
*/

void	ft_fatal(char *str);
int		ft_getch(void);

/*
**	key handling function pointers
*/

/*
const	t_readline_handler_tab	readline_keyhook[] = 
{
	{.key = K_LEFT, .action = &ft_key_left},
	{.key = K_BACK, .action = &ft_key_back},
	{.key = K_RIGHT, .action = &ft_key_right},
	{.key = 0, .action = NULL}
};
*/

#endif
