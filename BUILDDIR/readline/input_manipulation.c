/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input_manipulation.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/27 22:53:54 by abiri             #+#    #+#             */
/*   Updated: 2019/02/05 19:45:13 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_readline.h"

void	ft_getch_prepare(void)
{
	int				ret;
	struct termios	new_opts;

	ret = tcgetattr(STDIN_FILENO, &new_opts);
	new_opts.c_lflag &= ~(ICANON | ECHO | ECHOE | ECHOK
			| ECHONL | ECHOPRT | ECHOKE | ICRNL);
	ret += tcsetattr(STDIN_FILENO, TCSANOW, &new_opts);
	if (ret < 0)
		ft_fatal("Cannot modify terminal state\n");
}

int		ft_getch(void)
{
	int	c;

	c = 0;
	ft_getch_prepare();
	read(0, &c, 4);
	return (c);
}
