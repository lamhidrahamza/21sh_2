/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_keys.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/26 21:47:53 by abiri             #+#    #+#             */
/*   Updated: 2019/02/07 21:38:02 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_KEYS_H
# define FT_KEYS_H

# define K_UP 4283163
# define K_DOWN 4348699
# define K_LEFT 4479771
# define K_RIGHT 4414235
# define K_DEL 2117294875
# define K_BACK 127
# define K_ESC 27
# define K_ENTER 10
# define K_SPACE 32
# define K_HELP 104
# define K_PAGEDOWN 2117491483
# define K_PAGEUP 2117425947
# define K_HOME 4741915
# define K_END 4610843

#endif
