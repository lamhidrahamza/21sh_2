/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_termcaps.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/27 23:10:51 by abiri             #+#    #+#             */
/*   Updated: 2019/02/04 02:09:13 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_TERMCAPS_H
# define FT_TERMCAPS_H

# define CLEAR_SCR "cl"
# define ALT_SCR_ON "ti"
# define ALT_SCR_OFF "te"
# define GET_WIDTH "co"
# define GET_HEIGHT "li"

/*
** cursor termcaps
*/

# define MOVE_CURSOR "cm"
# define SAVE_CURSOR "sc"
# define RECOVER_CURSOR "rc"
# define HIDE_CURSOR "vi"
# define SHOW_CURSOR "ve"
# define ONE_UP "up"
# define ONE_DOWN "do"
# define ONE_RIGHT "nd"
# define ONE_LEFT "le"

/*
**	some functions used to facilitate working with termcaps
*/

int		ft_apply_cap(char *cap);
int		ft_refreshcap_value(char *cap);
void	ft_n_cap(char *cap, int times);

#endif
