# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/12/15 20:38:11 by abiri             #+#    #+#              #
#    Updated: 2019/03/14 15:05:27 by abiri            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

_lCYAN = \x1b[96m
_lYELLOW = \x1b[93m
_lGREEN = \x1b[92m
_lBLUE = \x1b[94m
_RED = \x1b[31m
_BOLD = \x1b[1m
_END = \x1b[0m

NAME = 21sh
SRC = src/core/main.c\
	  src/core/ft_alias.c\
	  src/core/ft_builtin.c\
	  src/core/ft_builtin_ext.c\
	  src/core/ft_cmdline.c\
	  src/core/ft_envbasic.c\
	  src/core/ft_envlib.c\
	  src/core/ft_interpret.c\
	  src/core/ft_signals.c\
	  src/core/ft_builtin_cd.c\
	  src/ft_match/ft_match.c\
	  src/ft_match/ft_match_parts.c\
	  src/ft_parse/parse.c\
	  src/ft_postinterpret/ft_prepare.c\
	  src/ft_split/arg_manipulation.c\
	  src/ft_split/ft_split_args.c\
	  src/ft_split/ft_split_escapers.c\
	  src/read_line/addition.c\
	  src/read_line/delete.c\
	  src/read_line/ft_read_line.c\
	  src/read_line/history.c\
	  src/read_line/home_end.c\
	  src/read_line/initial.c\
	  src/read_line/move_by_lines.c\
	  src/read_line/move_by_word.c\
	  src/read_line/moving_cursor.c\
	  src/read_line/output.c\
	  src/read_line/quotes.c\
	  src/read_line/reset_position.c\
	  src/read_line/selection1.c\
	  src/read_line/selection2.c\
	  src/read_line/termcap.c\
	  src/read_line/copy_paste.c\
	  src/read_line/ctrl_d.c\
	  src/read_line/auto_completion.c

FLAGS = -Wall -Werror -Wextra
INCLUDES = -I ./libft\
		   -I ./readline\
		   -I ./inc
LIBRARIES = libft/libft.a\
			libft/ft_printf/libftprintf.a\
			-ltermcap
DELAY = 0

all : $(NAME)
$(NAME):

	@echo "$(_BOLD)	                                               ";
	@echo "	                                               ";
	@echo "	      ,----,       ,---,             ,---,     ";
	@echo "	    .'   .' \   ,\`--.' |           ,--.' |     ";
	@echo "	  ,----,'    | /    /  :           |  |  :     ";
	@echo "	  |    :  .  ;:    |.' '  .--.--.  :  :  :     ";
	@echo "	  ;    |.'  / \`----':  | /  /    ' :  |  |,--. ";
	@echo "	  \`----'/  ;     '   ' ;|  :  /\`./ |  :  '   | ";
	@echo "	    /  ;  /      |   | ||  :  ;_   |  |   /' : ";
	@echo "	   ;  /  /-,     '   : ; \  \    \`.'  :  | | | ";
	@echo "	  /  /  /.\`|     |   | '  \`----.   \  |  ' | : ";
	@echo "	./__;      :     '   : | /  /\`--'  /  :  :_:,' ";
	@echo "	|   :    .'      ;   |.''--'.     /|  | ,'     ";
	@echo "	;   | .'         '---'    \`--'---' \`--''       ";
	@echo "	\`---'                                          ";
	@echo "	                                               $(_END)";
	@echo "gcc $(_lYELLOW)$(FLAGS)$(_END) $(_lCYAN)$(SRC)$(_END)\n$(_lGREEN)$(LIBRARIES)$(_END) -I$(_RED)./inc$(_END)$(_RED)$(INCLUDES)$(_END) -o $(_lBLUE)$(NAME)$(_lEND)$(_RED)\n"
	@gcc $(FLAGS) $(SRC) $(LIBRARIES) $(INCLUDES) -o $(NAME)
clean:
	@echo "\n$(_lCYAN)Makefile :$(_END) will delete $(_RED)$(SRC:.c=.o)$(_END)"
	@echo "starting in $(DELAY) sec, $(_RED)press Ctrl-c to abort$(_END)"
	@sleep $(DELAY)
	@rm -f $(SRC:.c=.o)
fclean: clean
	@echo "\n$(_lCYAN)Makefile :$(_END) will delete $(_RED)$(NAME)$(_END)"
	@echo "starting in $(DELAY) sec, $(_RED)press Ctrl-c to abort$(_END)"
	@sleep $(DELAY)
	@rm -f $(NAME)
fast:
	@rm -f $(NAME)
	@rm -f $(SRC:.c=.o)
	@gcc $(FLAGS) $(SRC) $(LIBRARIES) -I./inc -I$(INCLUDES) -o $(NAME)
re: fclean all
