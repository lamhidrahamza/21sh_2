/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strfreejoin.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@pm.me>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/26 14:51:13 by abiri             #+#    #+#             */
/*   Updated: 2019/03/26 14:52:54 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char					*ft_strfreejoin(char *s1, char *s2)
{
	char			*result;
	unsigned	int	len;
	unsigned	int	i;
	unsigned	int	j;

	j = 0;
	i = 0;
	if (!s1 && !s2)
		return (NULL);
	else if (!s1)
		return (ft_strdup(s2));
	else if (!s2)
		return (ft_strdup(s1));
	len = ft_strlen(s1);
	if ((result = malloc(len + ft_strlen(s2) + 1)))
	{
		ft_strcpy(result, s1);
		ft_strcpy(result + len, s2);
	}
	free(s1);
	free(s2);
	return (result);
}
