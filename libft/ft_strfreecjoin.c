/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strfreecjoin.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@pm.me>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/26 15:47:36 by abiri             #+#    #+#             */
/*   Updated: 2019/03/26 15:48:07 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strfreecjoin(char *s1, char join, char *s2)
{
	char	*result;
	size_t	size;
	size_t	middle;

	if (!s1 || !s2)
		return (NULL);
	middle = ft_strlen(s1);
	size = middle + ft_strlen(s2) + 2;
	if (!(result = malloc(size)))
		return (NULL);
	result = ft_strcpy(result, s1);
	result[middle] = join;
	ft_strcpy(result + middle + 1, s2);
	free(s1);
	free(s2);
	return (result);
}
