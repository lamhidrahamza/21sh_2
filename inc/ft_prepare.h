/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_prepare.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/22 19:51:50 by abiri             #+#    #+#             */
/*   Updated: 2019/07/16 11:16:21 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PREPARE_H
# define FT_PREPARE_H
# define NO_FD -1
# define REDIR_IN 0
# define REDIR_OUT 1
# define REDIR_SIMPLE 1
# define REDIR_DOUBLE 2

/*
**	this struct will be holding redirection to file or another fd
*/

typedef struct	s_redirect
{
	int					fdfrom;
	int					fdto;
	char				direction;
	char				type;
	char				*filename;
	struct s_redirect	*next;
}				t_redirect;

/*
**	in here i will define the struct that will hold information
**		about the current command 
*/

typedef struct	s_command
{
	char				**argv;
	t_redirect			*redirections;
	t_redirect			*lastredirection;
	struct	s_command	*next;
}				t_command;

/*
**	this struct will hold informations about the command line
**		and will also contain a list of commands
*/

typedef	struct	s_interpret
{
	t_command			*firstcmd;
	t_command			*lastcmd;
	struct s_interpret	*next;
}				t_interpret;

/*
**	this struct will contain a list of commands connected by pipes
*/

typedef struct	s_cmdline
{
	t_interpret	*first;
	t_interpret	*last;
}				t_cmdline;

/*
**	function prototypes
*/

t_interpret		*post_interpret_args(t_arg *args);
void			post_interpret_fd(t_interpret *cmd, t_arg *args);
#endif
