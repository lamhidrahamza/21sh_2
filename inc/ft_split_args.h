/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_args.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/21 22:24:27 by abiri             #+#    #+#             */
/*   Updated: 2019/03/26 15:23:23 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SPLIT_ARGS_H
# define FT_SPLIT_ARGS_H
# include "arg_manipulation.h"

int		ft_getarglen(char *arg, int start, int length);
char	*ft_get_args(char *arg, int start, int len);
t_arg	*ft_splitargs(char *line, int *status);

#endif
