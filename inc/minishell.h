/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/17 14:29:23 by abiri             #+#    #+#             */
/*   Updated: 2019/03/26 20:36:51 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef _MINISHELL_H
# define _MINISHELL_H
# include "libft.h"
# include <fcntl.h>
# include <errno.h>

char				*g_prompt;
int					g_sigintstat;

typedef struct		s_entry
{
	char			*key;
	char			*value;
	struct s_entry	*next;
}					t_entry;

typedef struct		s_dict
{
	size_t			size;
	t_entry			*first;
	t_entry			*last;
}					t_dict;

char				*ft_getpath(char *envpath, char *execname);
char				**ft_getargs(char *cmd);
int					ft_execright(char *path);
char				**ft_shellvars(char **args, t_dict *envlist);
void				ft_interpret(char *command, t_dict *envlist);
void				ft_exec(char **args, t_dict *envlist);
void				ft_source(t_dict *envlist, char *otherpath);
void				ft_setsignals();
void				ft_printpathstatus(int status, char *execname);

/*
**	builtin functions
*/

int					ft_echo(char **args);
int					ft_builtin_cd(char **args, t_dict *envlist);
int					ft_builtin_test(char **args, t_dict *envlist);
int					ft_builtin(char **args, t_dict *envlist);
int					ft_builtin_setenv(char **args, t_dict *envlist);
int					ft_builtin_error(char **args,
		char *progname, int min, size_t max);
int					ft_builtin_source(char **args, t_dict *envlist);
char				**ft_getalias(char **args);

/*
**	environment manipulation functions
*/

t_entry				*ft_newenv(const char *key, const char *value);
t_entry				*ft_addenv(t_entry *new, t_dict *head);
t_entry				*ft_findenv(t_dict *envlist, char *key);
char				*ft_getenv(t_dict *env, char *key);
char				**ft_getenvtab(t_dict *envlist, char *execpath);
void				ft_setenv(t_dict *envlist, char *key, char *value);
void				ft_unsetenv(t_dict *envlist, char *key);
void				ft_envfree(t_entry *entry);
void				ft_dictfree(t_dict *dict);
int					ft_env(t_dict *envlist);

/*
**	essential shell funcitons
*/

int					ft_shellmain(char **line, t_dict *envlist);

#endif
