/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   jojwahdsh.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlamhidr <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/05 21:27:03 by hlamhidr          #+#    #+#             */
/*   Updated: 2019/07/18 12:33:48 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef READ_LINE_H
# define READ_LINE_H

# include <unistd.h>
# include <stdio.h>
# include <sys/wait.h>
# include <dirent.h>
# include <sys/stat.h>
# include <string.h>
# include <fcntl.h>
# include <termios.h>
# include <term.h>
# include <stdlib.h>
# include <curses.h>
# include <sys/ioctl.h>
# include "libft.h"

# define MAX_HISTORY 20

# define ENTER(x) (x[0] == '\n' && x[1] == 0 && x[2] == 0 && x[3] == 0 && x[4] == 0 && x[5] == 0)
# define DEL(x) (x[0] == 127 && x[1] == 0 && x[2] == 0 && x[3] == 0 && x[4] == 0 && x[5] == 0)
# define UP(x) (x[0] == 27 && x[1] == '[' && x[2] == 'A' && x[3] == 0 && x[4] == 0 && x[5] == 0)
# define DO(x) (x[0] == 27 && x[1] == '[' && x[2] == 'B' && x[3] == 0 && x[4] == 0 && x[5] == 0)
# define RI(x) (x[0] == 27 && x[1] == '[' && x[2] == 'C' && x[3] == 0 && x[4] == 0 && x[5] == 0)
# define LE(x) (x[0] == 27 && x[1] == '[' && x[2] == 'D' && x[3] == 0 && x[4] == 0 && x[5] == 0)
# define SEL_RI(x) (x[0] == -30 && x[1] == -119 && x[2] == -91 && x[3] == 0 && x[4] == 0 && x[5] == 0)
# define SEL_LE(x) (x[0] == -30 && x[1] == -119 && x[2] == -92 && x[3] == 0 && x[4] == 0 && x[5] == 0)
# define COPY(x) (x[0] == -61 && x[1] == -89 && x[2] == 0 && x[3] == 0 && x[4] == 0 && x[5] == 0)
# define PASTE(x) (x[0] == -30 && x[1] == -120 && x[2] == -102 && x[3] == 0 && x[4] == 0 && x[5] == 0)
# define CUT(x) (x[0] == -30 && x[1] == -119 && x[2] == -120 && x[3] == 0 && x[4] == 0 && x[5] == 0)
# define LE_WOR(x) (x[0] == -59 && x[1] == -109 && x[2] == 0 && x[3] == 0 && x[4] == 0 && x[5] == 0)
# define RI_WOR(x) (x[0] == -30 && x[1] == -120 && x[2] == -111 && x[3] == 0 && x[4] == 0 && x[5] == 0)
# define HOME(x) (x[0] == 27 && x[1] == 91 && x[2] == 72 && x[3] == 0 && x[4] == 0 && x[5] == 0)
# define END(x) (x[0] == 27 && x[1] == 91 && x[2] == 70 && x[3] == 0 && x[4] == 0 && x[5] == 0)
# define C_UP(x) (x[0] == 27 && x[1] == 91 && x[2] == 49 && x[3] == 59 && x[4] == 53 && x[5] == 65)
# define C_DO(x) (x[0] == 27 && x[1] == 91 && x[2] == 49 && x[3] == 59 && x[4] == 53 && x[5] == 66)
# define CTRL_D(x) (x[0] == 4 && x[1] == 0 && x[2] == 0 && x[3] == 0 && x[4] == 0 && x[5] == 0)
# define TAB(x) (x[0] == '\t' && x[1] == 0 && x[2] == 0 && x[3] == 0 && x[4] == 0 && x[5] == 0)

typedef struct	s_cursor
{
	int	index;
	int p;
	int x;
	int y;
	int num_lines;
	int num_col;
	int *end;
	char *cmd;
	char **s;
}				t_cursor;

typedef	struct	s_select
{
	int start;
	int end;
	char *save;
}				t_select;

typedef	struct	s_history
{
	char		**history;
	int			his_count;
	char		*path;
}				t_history;

t_cursor pos1;

int		g_sign;


int				ft_indexofchar(char *str, int c);
char			*ft_read_line(t_history *his, t_select *select, int p);
void			ft_free_tab(char **tableau);
int				ft_tablen(char **tb);
int				ft_valid_quote(char *line);
void			ft_catch_signal(int signal);
int				ft_numberof(char *str, int c);
void			ft_see_touch(char *t, char *s, t_cursor *pos, t_select *select);
int				ft_get_size_windz(void);
int				ft_set_termcap(void);
int				ft_get_y_position(void);
void			ft_stock_history(char **history, char *line, int his_count);
int				my_outc(int c);
void			ft_selection(char *s, t_cursor *pos, char *key, t_select *select);
int				ft_get_num_of_lines_add(int len, int num_col);
void			ft_move_left(int n);
void			ft_movecur_up_and_right(int up, int size);
int				ft_get_num_of_lines(int num_col, char *s, int p);
void			ft_set_last_position(t_cursor pos, int num_lines);
void			ft_move_cursor_zero(t_cursor pos);
void			ft_copy_paste(char *buf, char **s, t_cursor *pos, t_select *select);
void			ft_mmmm(int **d);
void			ft_putstr_term(int num_col, char *s, t_cursor *pos);
void			ft_get_end_of_line_pos(t_cursor *pos, char *s, int num_col);
void			ft_get_new_pos(t_cursor *pos, int len_sa);
void			ft_move_by_word(t_cursor *pos, char *s, char *buf);
void			ft_home_end(t_cursor *pos, char *s, char *buf);
void			ft_move_by_lines(t_cursor *pos, char *s, char *buf);
void			ft_quotes(char **line, t_select *select, int q);
char			**ft_alloc_tab(void);
void			ft_win_change(int signal);
void			ft_print_with_reverse_mode(char *s, int start, int end, t_cursor *pos);
void			ft_left_selection(char *s, t_cursor *pos, t_select *select);
void			ft_remove_selections(t_cursor *pos, t_select *select, char *s);
char			*ft_delcolomn(char *s, t_cursor *pos);
void			ft_initial(char **s, int p);
void			ft_stock_history(char **history, char *line, int his_count);
void			ft_print_history(t_history *his, char *buf, char **s, t_cursor *pos);
char			*ft_line_edd(char *s, t_cursor *pos, char c);
void			ft_print_touch_and_join(t_cursor *pos, char *buf, char **s);
void			ft_move_right(int n);
char			*ft_ctrl_d(t_cursor *pos, t_history *his, t_select *select, char *s);
char			*ft_auto_completion(t_cursor *pos, t_history *his, char *s);

#endif

