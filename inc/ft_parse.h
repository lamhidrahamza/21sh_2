/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/05 23:49:51 by abiri             #+#    #+#             */
/*   Updated: 2019/07/18 13:28:40 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PARSE_H
# define FT_PARSE_H
# include "ft_match.h"
# include "ft_split_args.h"
# include "ft_split_macros.h"
# include "ft_split_escapers.h"
# define TYPE_SEMICOLON 1
# define TYPE_REDIRECT_IN 2
# define TYPE_PIPE 4
# define TYPE_REDIRECT_OUT 8
# define TYPE_FILEREAD 16
# define TYPE_FILEWRITE 32
# define TYPE_HEREDOC 64
# define TYPE_COMMAND 128
# define TYPE_NONE 256
# define FILE_ERROR -1
# define PARSE_ERROR 0
# define PARSE_OK 1
# define PARSE_INCOMPLETE 2

typedef	int 	(t_syntax_verif)(t_arg *);

typedef struct	s_syntax_element
{
	int				type;
	t_syntax_verif	*func;
}				t_syntax_element;

int	ft_parse_args(t_arg *parts);

#endif
