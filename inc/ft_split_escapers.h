/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_escapers.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/21 22:37:49 by abiri             #+#    #+#             */
/*   Updated: 2019/03/13 15:33:30 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SPLIT_ESCAPERS_H
# define FT_SPLIT_ESCAPERS_H

/*
**	Functions for detecting spliting characters
*/

int		ft_isbackslash(char *str, int index);
int		ft_fromcharset(char *str, int index, char *charset);
char	*ft_vip_args(char *arg, int index, int *offset);
char	*ft_get_redirection(char *line, int index, int *before, int startindex);

#endif
