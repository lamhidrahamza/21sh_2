/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   arg_manipulation.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@protonmail>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/17 22:16:38 by abiri             #+#    #+#             */
/*   Updated: 2019/02/22 18:01:53 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ARG_MANIPULATION_H
# define ARG_MANIPULATION_H

typedef struct	s_arg
{
	char			*content;
	int				type;
	struct s_arg	*next;
}				t_arg;

void			ft_add_arg(t_arg **head, t_arg *new);
t_arg			*ft_new_arg(char *content, int escape);

#endif
