/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_interpret.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiri <kerneloverseer@pm.me>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/23 14:39:26 by abiri             #+#    #+#             */
/*   Updated: 2019/07/06 11:30:08 by abiri            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_INTERPRET_H
# define FT_INTERPRET_H
# include "arg_manipulation.h"
# include "ft_parse.h"
# include "ft_prepare.h"
# include "ft_split_args.h"
# include <signal.h>
# define PIPE_R 0
# define PIPE_W 1
# define PIPE_IN 1
# define PIPE_OUT 0
# define CMD_FIRST 1
# define CMD_LAST 2
# define KILL_CHILDREN -2

#endif
